This is the alternative version of the Physical Presence Calculator for
permanent residents applying for Canadian citizenship.  For the citizenship
application please use [the official
calculator](https://eservices.cic.gc.ca/rescalc/resCalcStartNew.do).


### The problem I am trying to solve

The official calculator isn't sufficiently interactive. You need to provide all
the data upfront and then it will show you the results. Also the official
calculator tells you *if* you can apply and I am more interested in *when* I
can apply.

The downside is that the current calculator might be outdated and not
reflecting the latest regulations. The calculator implements the physical
presence rules as of June 1st, 2018.


## Development instructions

### Local development

    yarn start

### Tests

    yarn test

### Deployment

    yarn build
