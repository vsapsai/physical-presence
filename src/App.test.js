import React from 'react';
import ReactDOM from 'react-dom';
import ReactTestUtils from 'react-dom/test-utils';
import App, { expectedApplicationDate, canonicalState } from './App';

// For accessing components.
import Calendar from './Calendar';
import DateInputField from './DateInputField';
import ImportExportModal from './ImportExportModal';

function state(overrides) {
    let defaultState = {
        prStartDate: null,
        temporaryResidencePeriods: [],
        absencePeriods: [],
    };
    let state = Object.assign({}, defaultState, overrides || {});
    return state;
}

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});

describe('rendered component', () => {
    var componentTree, calendar;

    beforeEach(() => {
        componentTree = ReactTestUtils.renderIntoDocument(
            <App />
        );
        calendar = ReactTestUtils.findRenderedComponentWithType(
            componentTree, Calendar);
    });

    function clickAddPeriod(isAbsencePeriod) {
        let addPeriodButtons = ReactTestUtils.scryRenderedDOMComponentsWithClass(
            componentTree, 'TableButton');
        let buttonIndex = isAbsencePeriod ? 1 : 0;
        ReactTestUtils.Simulate.click(addPeriodButtons[buttonIndex]);
    }

    function removeInputFieldFocus() {
        let element = ReactTestUtils.findRenderedDOMComponentWithClass(
            componentTree, 'MultiColumnPanel');
        ReactTestUtils.Simulate.click(element);
    }

    describe('date selection', () => {
        it('focus in PR start date field should make calendar selectable', () => {
            expect(calendar.props.selectable).toBeFalsy();
            let dateInputField = ReactTestUtils.findRenderedComponentWithType(
                componentTree, DateInputField);
            dateInputField.props.onFocus();
            expect(calendar.props.selectable).toBeTruthy();
        });

        it('selecting PR start date in calendar should be reflected in the calendar', () => {
            expect(calendar.props.selection).toEqual([]);
            let dateInputField = ReactTestUtils.findRenderedComponentWithType(
                componentTree, DateInputField);
            dateInputField.props.onFocus();
            calendar.props.onSelectDate(new Date(2017, 9, 13));
            let selection = calendar.props.selection;
            expect(selection.length).toEqual(3);
            expect(selection[0].type).toEqual('single');
            expect(selection[0].date.getFullYear()).toEqual(2017);
            expect(selection[0].date.getMonth()).toEqual(9);
            expect(selection[0].date.getDate()).toEqual(13);
            expect(selection[0].className).toEqual('prStartDate-selected');
            expect(selection[1].type).toEqual('single');
            expect(selection[1].className).toEqual('applicationDate-selected');
            expect(selection[2].type).toEqual('range');
            expect(selection[2].className).toEqual('permanentResidencePeriods-selected');
        });

        it('should order start and end dates in periods', () => {
            clickAddPeriod();
            let inputFields = ReactTestUtils.scryRenderedComponentsWithType(
                componentTree, DateInputField);
            inputFields[1].props.onFocus();
            calendar.props.onSelectDate(new Date(2017, 9, 15));
            inputFields[2].props.onFocus();
            calendar.props.onSelectDate(new Date(2017, 9, 13));
            let selection = calendar.props.selection;
            expect(selection.length).toEqual(1);
            expect(selection[0].type).toEqual('range');
            expect(selection[0].start.getDate()).toEqual(13);
            expect(selection[0].end.getDate()).toEqual(15);
        });

        it('auto-select another period component', () => {
            clickAddPeriod();
            let inputFields = ReactTestUtils.scryRenderedComponentsWithType(
                componentTree, DateInputField);
            inputFields[2].props.onFocus();
            calendar.props.onSelectDate(new Date(2017, 9, 13));
            // Select another component.
            expect(inputFields[1].props.selected).toBeTruthy();
            expect(inputFields[2].props.selected).toBeFalsy();
            // But only if it's empty.
            calendar.props.onSelectDate(new Date(2017, 9, 10));
            expect(inputFields[1].props.selected).toBeFalsy();
            expect(inputFields[2].props.selected).toBeFalsy();
        });
    });

    describe('pending range selection', () => {
        it('should show for range', () => {
            clickAddPeriod();
            expect(calendar.props.pendingRangeSelection).toBeNull();
            calendar.props.onSelectDate(new Date(2017, 9, 13));
            expect(calendar.props.pendingRangeSelection).not.toBeNull();
            let pendingDate = calendar.props.pendingRangeSelection;
            expect(pendingDate.getFullYear()).toEqual(2017);
            expect(pendingDate.getMonth()).toEqual(9);
            expect(pendingDate.getDate()).toEqual(13);
        });

        it('should not show for single date', () => {
            let inputFields = ReactTestUtils.scryRenderedComponentsWithType(
                componentTree, DateInputField);
            inputFields[0].props.onFocus();
            expect(calendar.props.pendingRangeSelection).toBeNull();
            calendar.props.onSelectDate(new Date(2017, 9, 13));
            expect(calendar.props.pendingRangeSelection).toBeNull();
        });
    });

    describe('sort periods', () => {
        function addPeriod(start, end) {
            clickAddPeriod();
            let inputFields = ReactTestUtils.scryRenderedComponentsWithType(
                componentTree, DateInputField);
            if (start) {
                inputFields[inputFields.length - 2].props.onFocus();
                calendar.props.onSelectDate(start);
            }
            if (end) {
                inputFields[inputFields.length - 1].props.onFocus();
                calendar.props.onSelectDate(end);
            }
            removeInputFieldFocus();
        }

        var app;
        beforeEach(() => {
            app = ReactTestUtils.findRenderedComponentWithType(
                componentTree, App);
        });

        it('non-empty periods', () => {
            addPeriod(new Date(2017, 9, 13), new Date(2017, 9, 15));
            addPeriod(new Date(2017, 9, 3), new Date(2017, 9, 5));
            let periods = app.state.temporaryResidencePeriods;
            expect(periods.length).toEqual(2);
            expect(periods[0].start.getDate()).toEqual(3);
            expect(periods[0].end.getDate()).toEqual(5);
            expect(periods[1].start.getDate()).toEqual(13);
            expect(periods[1].end.getDate()).toEqual(15);
        });

        it('non-empty periods with the same start date', () => {
            addPeriod(new Date(2017, 9, 13), new Date(2017, 9, 25));
            addPeriod(new Date(2017, 9, 13), new Date(2017, 9, 15));
            let periods = app.state.temporaryResidencePeriods;
            expect(periods.length).toEqual(2);
            expect(periods[0].start.getDate()).toEqual(13);
            expect(periods[0].end.getDate()).toEqual(15);
            expect(periods[1].start.getDate()).toEqual(13);
            expect(periods[1].end.getDate()).toEqual(25);
        });

        it('one period has only start', () => {
            addPeriod(new Date(2017, 9, 13), new Date(2017, 9, 15));
            addPeriod(new Date(2017, 9, 3), null);
            let periods = app.state.temporaryResidencePeriods;
            expect(periods.length).toEqual(2);
            expect(periods[0].start.getDate()).toEqual(3);
            expect(periods[0].end).toBeNull();
            expect(periods[1].start.getDate()).toEqual(13);
            expect(periods[1].end.getDate()).toEqual(15);
        });

        it('one period has only end', () => {
            addPeriod(new Date(2017, 9, 13), new Date(2017, 9, 15));
            addPeriod(null, new Date(2017, 9, 3));
            let periods = app.state.temporaryResidencePeriods;
            expect(periods.length).toEqual(2);
            expect(periods[0].start).toBeNull();
            expect(periods[0].end.getDate()).toEqual(3);
            expect(periods[1].start.getDate()).toEqual(13);
            expect(periods[1].end.getDate()).toEqual(15);
        });

        it('one period has single component equal to another period component', () => {
            addPeriod(null, new Date(2017, 9, 13));
            addPeriod(new Date(2017, 9, 13), new Date(2017, 9, 15));
            // Prefer to put half-specified periods in the end, given
            // everything else is the same.
            let periods = app.state.temporaryResidencePeriods;
            expect(periods.length).toEqual(2);
            expect(periods[0].start.getDate()).toEqual(13);
            expect(periods[0].end.getDate()).toEqual(15);
            expect(periods[1].start).toBeNull();
            expect(periods[1].end.getDate()).toEqual(13);
        });

        it('one period is empty', () => {
            addPeriod(null, null);
            addPeriod(new Date(2017, 9, 13), new Date(2017, 9, 15));
            let periods = app.state.temporaryResidencePeriods;
            expect(periods.length).toEqual(2);
            expect(periods[0].start.getDate()).toEqual(13);
            expect(periods[0].end.getDate()).toEqual(15);
            expect(periods[1].start).toBeNull();
            expect(periods[1].end).toBeNull();
        });
    });

    it('delete a period', () => {
        var app = ReactTestUtils.findRenderedComponentWithType(
            componentTree, App);
        clickAddPeriod();
        clickAddPeriod();
        let inputFields = ReactTestUtils.scryRenderedComponentsWithType(
            componentTree, DateInputField);
        inputFields[1].props.onFocus();
        calendar.props.onSelectDate(new Date(2017, 9, 1));
        inputFields[3].props.onFocus();
        calendar.props.onSelectDate(new Date(2017, 9, 2));
        expect(app.state.temporaryResidencePeriods.length).toEqual(2);

        let iconButtons = ReactTestUtils.scryRenderedDOMComponentsWithClass(
            componentTree, 'IconButton');
        ReactTestUtils.Simulate.click(iconButtons[0]);
        expect(app.state.temporaryResidencePeriods.length).toEqual(1);
        expect(app.state.temporaryResidencePeriods[0].start.getDate()).toEqual(2);
    });

    describe('warnings about periods', () => {
        function getWarnings() {
            let warnings = ReactTestUtils.scryRenderedDOMComponentsWithClass(
                componentTree, 'Warning');
            return warnings;
        }

        describe('incomplete periods', () => {
            it('incomplete periods with focus', () => {
                clickAddPeriod();
                calendar.props.onSelectDate(new Date(2017, 9, 13));
                let warnings = getWarnings();
                expect(warnings.length).toEqual(0);
            });

            it('incomplete periods without focus', () => {
                clickAddPeriod();
                calendar.props.onSelectDate(new Date(2017, 9, 13));
                removeInputFieldFocus();
                let warnings = getWarnings();
                expect(warnings.length).toEqual(1);
                expect(warnings[0].textContent).toEqual('Incomplete periods are ignored');
            });

            it('entirely empty period', () => {
                clickAddPeriod();
                removeInputFieldFocus();
                let warnings = getWarnings();
                expect(warnings.length).toEqual(0);
            });

            it('entirely specified period', () => {
                clickAddPeriod();
                calendar.props.onSelectDate(new Date(2017, 9, 13));
                calendar.props.onSelectDate(new Date(2017, 9, 17));
                removeInputFieldFocus();
                let warnings = getWarnings();
                expect(warnings.length).toEqual(0);
            });
        });

        describe('overlapping periods', () => {
            it('multiple overlaps', () => {
                clickAddPeriod();
                calendar.props.onSelectDate(new Date(2017, 9, 1));
                calendar.props.onSelectDate(new Date(2017, 9, 30));
                clickAddPeriod();
                calendar.props.onSelectDate(new Date(2017, 9, 5));
                calendar.props.onSelectDate(new Date(2017, 9, 8));
                clickAddPeriod();
                calendar.props.onSelectDate(new Date(2017, 9, 12));
                calendar.props.onSelectDate(new Date(2017, 9, 18));
                removeInputFieldFocus();
                let warnings = getWarnings();
                expect(warnings.length).toEqual(2);
                expect(warnings[0].textContent).toEqual('Overlapping periods are counted only once');
                expect(warnings[1].textContent).toEqual('Overlapping periods are counted only once');
            });

            it('single day', () => {
                clickAddPeriod();
                calendar.props.onSelectDate(new Date(2017, 9, 5));
                calendar.props.onSelectDate(new Date(2017, 9, 8));
                clickAddPeriod();
                calendar.props.onSelectDate(new Date(2017, 9, 8));
                calendar.props.onSelectDate(new Date(2017, 9, 9));
                removeInputFieldFocus();
                let warnings = getWarnings();
                expect(warnings.length).toEqual(1);
                expect(warnings[0].textContent).toEqual('Overlapping periods are counted only once');
            });
        });

        it('temporary residence befor PR start date', () => {
            let dateInputField = ReactTestUtils.findRenderedComponentWithType(
                componentTree, DateInputField);
            dateInputField.props.onFocus();
            calendar.props.onSelectDate(new Date(2017, 9, 13));
            clickAddPeriod();
            calendar.props.onSelectDate(new Date(2017, 9, 10));
            calendar.props.onSelectDate(new Date(2017, 9, 15));
            removeInputFieldFocus();
            let warnings = getWarnings();
            expect(warnings.length).toEqual(1);
            expect(warnings[0].textContent).toEqual('Temporary residence after obtaining PR status is ignored');
        });
    });

    describe('import/export', () => {
        function clickExport() {
            let buttons = ReactTestUtils.scryRenderedDOMComponentsWithClass(
                componentTree, 'Button');
            ReactTestUtils.Simulate.click(buttons[0]);
        }

        function populateAllFields() {
            let dateInputField = ReactTestUtils.findRenderedComponentWithType(
                componentTree, DateInputField);
            dateInputField.props.onFocus();
            calendar.props.onSelectDate(new Date(2017, 9, 13));
            clickAddPeriod();
            calendar.props.onSelectDate(new Date(2017, 8, 10));
            calendar.props.onSelectDate(new Date(2017, 8, 15));
            clickAddPeriod(/*isAbsencePeriod=*/true);
            calendar.props.onSelectDate(new Date(2017, 9, 20));
            calendar.props.onSelectDate(new Date(2017, 9, 25));
            removeInputFieldFocus();
        }

        var modal;
        beforeEach(() => {
            modal = ReactTestUtils.findRenderedComponentWithType(
                componentTree, ImportExportModal);
        });

        it('export is pretty-printed', () => {
            let dateInputField = ReactTestUtils.findRenderedComponentWithType(
                componentTree, DateInputField);
            dateInputField.props.onFocus();
            calendar.props.onSelectDate(new Date(2017, 9, 13));
            clickExport();
            expect(modal.props.content).toEqual('{\n    "permanent_resident": "2017-10-13",\n    "temporary_resident": [],\n    "absent": []\n}');
        });

        it('export contains all fields', () => {
            populateAllFields();
            clickExport();
            let exportObject = JSON.parse(modal.props.content);
            expect(exportObject['permanent_resident']).toEqual('2017-10-13');
            expect(exportObject['temporary_resident'])
                .toEqual([{start: '2017-09-10', end: '2017-09-15'}]);
            expect(exportObject['absent'])
                .toEqual([{start: '2017-10-20', end: '2017-10-25'}]);
        });

        it('import rejects invalid input', () => {
            let errorMessage = modal.props.onImport('{"permanent_resident": "10/13/2017"}');
            expect(errorMessage).toBeTruthy();
            let app = ReactTestUtils.findRenderedComponentWithType(
                componentTree, App);
            expect(app.state.prStartDate).toBeNull();
        });

        it('import rejects wrong number of days in the month', () => {
            let errorMessage = modal.props.onImport('{"permanent_resident": "2017-09-31"}');
            expect(errorMessage).toBeTruthy();
            let app = ReactTestUtils.findRenderedComponentWithType(
                componentTree, App);
            expect(app.state.prStartDate).toBeNull();
        });

        it('import converts between one-based month and zero-based month', () => {
            let errorMessage = modal.props.onImport('{"permanent_resident": "2017-03-31"}');
            expect(errorMessage).toBeFalsy();
            let app = ReactTestUtils.findRenderedComponentWithType(
                componentTree, App);
            let prStartDate = app.state.prStartDate;
            expect(prStartDate.getFullYear()).toEqual(2017);
            expect(prStartDate.getMonth()).toEqual(2);
            expect(prStartDate.getDate()).toEqual(31);
        });

        it('export/impor roundtrips', () => {
            populateAllFields();
            clickExport();
            let errorMessage = modal.props.onImport(modal.props.content);
            expect(errorMessage).toBeFalsy();
            let app = ReactTestUtils.findRenderedComponentWithType(
                componentTree, App);
            let prStartDate = app.state.prStartDate;
            expect(prStartDate).not.toBeNull();
            expect(prStartDate.getFullYear()).toEqual(2017);
            expect(prStartDate.getMonth()).toEqual(9);
            expect(prStartDate.getDate()).toEqual(13);
            expect(app.state.temporaryResidencePeriods.length).toEqual(1);
            expect(app.state.absencePeriods.length).toEqual(1);
        });
    });
});

describe('canonicalState', () => {
    it('preserves prStartDate', () => {
        let original = state({prStartDate: new Date(2017, 9, 13)});
        let canonical = canonicalState(original);
        expect(canonical.prStartDate.getFullYear()).toEqual(2017);
        expect(canonical.prStartDate.getMonth()).toEqual(9);
        expect(canonical.prStartDate.getDate()).toEqual(13);
    });

    describe('no invalid periods', () => {
        let periods = [
            { start: new Date(2017, 9, 13) },
            { end: new Date(2017, 9, 13) },
            {},
            { start: new Date(2017, 9, 13), end: new Date(2017, 9, 15) }
        ];

        function verifyResultingPeriods(periods) {
            expect(periods.length).toEqual(1);
            expect(periods[0].start.getDate()).toEqual(13);
            expect(periods[0].end.getDate()).toEqual(15);
        }

        it('temporaryResidencePeriods', () => {
            let original = state({temporaryResidencePeriods: periods});
            let canonical = canonicalState(original);
            verifyResultingPeriods(canonical.temporaryResidencePeriods);
            // Original state stays unmodified.
            expect(original.temporaryResidencePeriods.length).toEqual(4);
        });

        it('absencePeriods', () => {
            let original = state({
                prStartDate: new Date(2017, 8, 1),
                absencePeriods: periods
            });
            let canonical = canonicalState(original);
            verifyResultingPeriods(canonical.absencePeriods);
            // Original state stays unmodified.
            expect(original.absencePeriods.length).toEqual(4);
        });
    });

    describe('no overlapping periods', () => {
        // ----
        //   ----
        describe('intersection', () => {
            let periods = [
                { start: new Date(2017, 9, 1), end: new Date(2017, 9, 4) },
                { start: new Date(2017, 9, 3), end: new Date(2017, 9, 6) },
            ];

            function verify(periodName, addPRStartDate) {
                let overrides = {};
                overrides[periodName] = periods;
                if (addPRStartDate) {
                    overrides.prStartDate = new Date(2017, 8, 1);
                }
                let canonical = canonicalState(state(overrides));
                let canonicalPeriods = canonical[periodName];
                expect(canonicalPeriods.length).toEqual(2);
                expect(canonicalPeriods[0].start.getDate()).toEqual(1);
                expect(canonicalPeriods[0].end.getDate()).toEqual(4);
                expect(canonicalPeriods[1].start.getDate()).toEqual(5);
                expect(canonicalPeriods[1].end.getDate()).toEqual(6);
            }

            it('temporaryResidencePeriods', () => {
                verify('temporaryResidencePeriods');
            });
            it('absencePeriods', () => {
                verify('absencePeriods', /*addPRStartDate =*/true);
            });
        });

        // ----
        //  --
        describe('inclusion', () => {
            let periods = [
                { start: new Date(2017, 9, 1), end: new Date(2017, 9, 4) },
                { start: new Date(2017, 9, 2), end: new Date(2017, 9, 3) },
            ];

            function verify(periodName, addPRStartDate) {
                let overrides = {};
                overrides[periodName] = periods;
                if (addPRStartDate) {
                    overrides.prStartDate = new Date(2017, 8, 1);
                }
                let canonical = canonicalState(state(overrides));
                let canonicalPeriods = canonical[periodName];
                expect(canonicalPeriods.length).toEqual(1);
                expect(canonicalPeriods[0].start.getDate()).toEqual(1);
                expect(canonicalPeriods[0].end.getDate()).toEqual(4);
            }

            it('temporaryResidencePeriods', () => {
                verify('temporaryResidencePeriods');
            });
            it('absencePeriods', () => {
                verify('absencePeriods', /*addPRStartDate =*/true);
            });
        });

        // ----
        //  ---
        //   ----
        describe('intersection & inclusion', () => {
            let periods = [
                { start: new Date(2017, 9, 1), end: new Date(2017, 9, 4) },
                { start: new Date(2017, 9, 2), end: new Date(2017, 9, 4) },
                { start: new Date(2017, 9, 3), end: new Date(2017, 9, 6) },
            ];

            function verify(periodName, addPRStartDate) {
                let overrides = {};
                overrides[periodName] = periods;
                if (addPRStartDate) {
                    overrides.prStartDate = new Date(2017, 8, 1);
                }
                let canonical = canonicalState(state(overrides));
                let canonicalPeriods = canonical[periodName];
                expect(canonicalPeriods.length).toEqual(2);
                expect(canonicalPeriods[0].start.getDate()).toEqual(1);
                expect(canonicalPeriods[0].end.getDate()).toEqual(4);
                expect(canonicalPeriods[1].start.getDate()).toEqual(5);
                expect(canonicalPeriods[1].end.getDate()).toEqual(6);
            }

            it('temporaryResidencePeriods', () => {
                verify('temporaryResidencePeriods');
            });
            it('absencePeriods', () => {
                verify('absencePeriods', /*addPRStartDate =*/true);
            });
        });

        // ---
        //   ---
        //     ---
        describe('multiple intersections', () => {
            let periods = [
                { start: new Date(2017, 9, 1), end: new Date(2017, 9, 3) },
                { start: new Date(2017, 9, 3), end: new Date(2017, 9, 5) },
                { start: new Date(2017, 9, 5), end: new Date(2017, 9, 7) },
            ];

            function verify(periodName, addPRStartDate) {
                let overrides = {};
                overrides[periodName] = periods;
                if (addPRStartDate) {
                    overrides.prStartDate = new Date(2017, 8, 1);
                }
                let canonical = canonicalState(state(overrides));
                let canonicalPeriods = canonical[periodName];
                expect(canonicalPeriods.length).toEqual(3);
                expect(canonicalPeriods[0].start.getDate()).toEqual(1);
                expect(canonicalPeriods[0].end.getDate()).toEqual(3);
                expect(canonicalPeriods[1].start.getDate()).toEqual(4);
                expect(canonicalPeriods[1].end.getDate()).toEqual(5);
                expect(canonicalPeriods[2].start.getDate()).toEqual(6);
                expect(canonicalPeriods[2].end.getDate()).toEqual(7);
            }

            it('temporaryResidencePeriods', () => {
                verify('temporaryResidencePeriods');
            });
            it('absencePeriods', () => {
                verify('absencePeriods', /*addPRStartDate =*/true);
            });
        });
    });

    it('no temporary residence after PR start date', () => {
        let original = state({
            prStartDate: new Date(2017, 9, 5),
            temporaryResidencePeriods: [
                { start: new Date(2017, 9, 1), end: new Date(2017, 9, 10) },
                { start: new Date(2017, 9, 15), end: new Date(2017, 9, 20) },
            ],
        });
        let canonical = canonicalState(original);
        expect(canonical.temporaryResidencePeriods.length).toEqual(1);
        expect(canonical.temporaryResidencePeriods[0].start.getDate()).toEqual(1);
        expect(canonical.temporaryResidencePeriods[0].end.getDate()).toEqual(4);
        expect(canonical.originalTemporaryResidencePeriods.length).toEqual(1);
        expect(canonical.originalTemporaryResidencePeriods[0].start.getDate()).toEqual(1);
        expect(canonical.originalTemporaryResidencePeriods[0].end.getDate()).toEqual(4);
        // Original state should stay unmodified.
        expect(original.temporaryResidencePeriods[0].start.getDate()).toEqual(1);
        expect(original.temporaryResidencePeriods[0].end.getDate()).toEqual(10);
    });

    describe('no absences during temporary residence', () => {
        it('absence splits temporary residence period', () => {
            let original = state({
                temporaryResidencePeriods: [
                    { start: new Date(2017, 9, 1), end: new Date(2017, 9, 15) },
                ],
                absencePeriods: [
                    { start: new Date(2017, 9, 6), end: new Date(2017, 9, 10) },
                ],
            });
            let canonical = canonicalState(original);
            expect(canonical.temporaryResidencePeriods.length).toEqual(2);
            expect(canonical.temporaryResidencePeriods[0].start.getDate()).toEqual(1);
            expect(canonical.temporaryResidencePeriods[0].end.getDate()).toEqual(5);
            expect(canonical.temporaryResidencePeriods[1].start.getDate()).toEqual(11);
            expect(canonical.temporaryResidencePeriods[1].end.getDate()).toEqual(15);
            expect(canonical.absencePeriods.length).toEqual(0);
            expect(canonical.originalTemporaryResidencePeriods.length).toEqual(1);
            expect(canonical.originalTemporaryResidencePeriods[0].start.getDate()).toEqual(1);
            expect(canonical.originalTemporaryResidencePeriods[0].end.getDate()).toEqual(15);
            expect(canonical.effectiveTemporaryResidenceAbsencePeriods.length).toEqual(1);
            expect(canonical.effectiveTemporaryResidenceAbsencePeriods[0].start.getDate()).toEqual(6);
            expect(canonical.effectiveTemporaryResidenceAbsencePeriods[0].end.getDate()).toEqual(10);
            // Original state should stay unmodified.
            expect(original.temporaryResidencePeriods.length).toEqual(1);
            expect(original.temporaryResidencePeriods[0].start.getDate()).toEqual(1);
            expect(original.temporaryResidencePeriods[0].end.getDate()).toEqual(15);
        });

        it('temporary residence and absence periods do not intersect', () => {
            let original = state({
                temporaryResidencePeriods: [
                    { start: new Date(2017, 9, 1), end: new Date(2017, 9, 5) },
                    { start: new Date(2017, 9, 11), end: new Date(2017, 9, 15) },
                ],
                absencePeriods: [
                    { start: new Date(2017, 9, 6), end: new Date(2017, 9, 10) },
                ],
            });
            let canonical = canonicalState(original);
            expect(canonical.temporaryResidencePeriods.length).toEqual(2);
            expect(canonical.temporaryResidencePeriods[0].start.getDate()).toEqual(1);
            expect(canonical.temporaryResidencePeriods[0].end.getDate()).toEqual(5);
            expect(canonical.temporaryResidencePeriods[1].start.getDate()).toEqual(11);
            expect(canonical.temporaryResidencePeriods[1].end.getDate()).toEqual(15);
            expect(canonical.absencePeriods.length).toEqual(0);
            expect(canonical.originalTemporaryResidencePeriods.length).toEqual(2);
            expect(canonical.effectiveTemporaryResidenceAbsencePeriods.length).toEqual(0);
        });

        it('temporary residence period is partially consumed by 2 absences', () => {
            let original = state({
                temporaryResidencePeriods: [
                    { start: new Date(2017, 9, 1), end: new Date(2017, 9, 10) },
                ],
                absencePeriods: [
                    { start: new Date(2017, 9, 3), end: new Date(2017, 9, 5) },
                    { start: new Date(2017, 9, 6), end: new Date(2017, 9, 10) },
                ],
            });
            let canonical = canonicalState(original);
            expect(canonical.temporaryResidencePeriods.length).toEqual(1);
            expect(canonical.temporaryResidencePeriods[0].start.getDate()).toEqual(1);
            expect(canonical.temporaryResidencePeriods[0].end.getDate()).toEqual(2);
            expect(canonical.absencePeriods.length).toEqual(0);
            expect(canonical.originalTemporaryResidencePeriods.length).toEqual(1);
            expect(canonical.originalTemporaryResidencePeriods[0].start.getDate()).toEqual(1);
            expect(canonical.originalTemporaryResidencePeriods[0].end.getDate()).toEqual(10);
            let effectiveAbsencePeriods = canonical.effectiveTemporaryResidenceAbsencePeriods;
            expect(effectiveAbsencePeriods.length).toEqual(2);
            expect(effectiveAbsencePeriods[0].start.getDate()).toEqual(3);
            expect(effectiveAbsencePeriods[0].end.getDate()).toEqual(5);
            expect(effectiveAbsencePeriods[1].start.getDate()).toEqual(6);
            expect(effectiveAbsencePeriods[1].end.getDate()).toEqual(10);
        });

        it('keeps absence periods only after the PR start date', () => {
            let original = state({
                prStartDate: new Date(2017, 9, 6),
                absencePeriods: [
                    { start: new Date(2017, 9, 1), end: new Date(2017, 9, 3) },
                    { start: new Date(2017, 9, 5), end: new Date(2017, 9, 7) },
                    { start: new Date(2017, 9, 9), end: new Date(2017, 9, 12) },
                ],
            });
            let canonical = canonicalState(original);
            expect(canonical.absencePeriods.length).toEqual(2);
            expect(canonical.absencePeriods[0].start.getDate()).toEqual(6);
            expect(canonical.absencePeriods[0].end.getDate()).toEqual(7);
            expect(canonical.absencePeriods[1].start.getDate()).toEqual(9);
            expect(canonical.absencePeriods[1].end.getDate()).toEqual(12);
            // Original state should stay unmodified.
            expect(original.absencePeriods.length).toEqual(3);
            expect(original.absencePeriods[1].start.getDate()).toEqual(5);
            expect(original.absencePeriods[1].end.getDate()).toEqual(7);
        });
    });
});

describe('expectedApplicationDate', () => {
    it('no PR status', () => {
        expect(expectedApplicationDate(state({prStartDate: null}))).toBeNull();
    });

    it('PR only', () => {
        let date = expectedApplicationDate(state({prStartDate: new Date(2017, 0, 1)}));
        expect(date.getFullYear()).toEqual(2020);
        expect(date.getMonth()).toEqual(0);
        expect(date.getDate()).toEqual(1);
    });

    it('PR only, with leap year', () => {
        let date = expectedApplicationDate(state({prStartDate: new Date(2015, 0, 1)}));
        expect(date.getFullYear()).toEqual(2017);
        expect(date.getMonth()).toEqual(11);
        expect(date.getDate()).toEqual(31);
    });

    it('with absence period', () => {
        let date = expectedApplicationDate(state({
            prStartDate: new Date(2017, 0, 1),
            absencePeriods: [ {start: new Date(2018, 0, 1), end: new Date(2018, 0, 31)} ],
        }));
        expect(date.getFullYear()).toEqual(2020);
        expect(date.getMonth()).toEqual(1);
        expect(date.getDate()).toEqual(1);
    });

    it('with absence period after application date', () => {
        let date = expectedApplicationDate(state({
            prStartDate: new Date(2017, 0, 1),
            absencePeriods: [ {start: new Date(2020, 1, 1), end: new Date(2020, 2, 15)} ],
        }));
        expect(date.getFullYear()).toEqual(2020);
        expect(date.getMonth()).toEqual(0);
        expect(date.getDate()).toEqual(1);
    });

    it('with temporary residence', () => {
        let date = expectedApplicationDate(state({
            prStartDate: new Date(2017, 1, 1),
            temporaryResidencePeriods: [
                {start: new Date(2017, 0, 1), end: new Date(2017, 0, 31)}
            ],
        }));
        expect(date.getFullYear()).toEqual(2020);
        expect(date.getMonth()).toEqual(0);
        expect(date.getDate()).toEqual(17);  // 15 days earlier
    });

    it('with temporary residence exceeding 2 years', () => {
        let date = expectedApplicationDate(state({
            prStartDate: new Date(2017, 0, 1),
            temporaryResidencePeriods: [
                {start: new Date(2014, 0, 1), end: new Date(2016, 11, 31)}
            ],
        }));
        expect(date.getFullYear()).toEqual(2019);
        expect(date.getMonth()).toEqual(0);
        expect(date.getDate()).toEqual(1);
    });

    it('with temporary residence and absence periods', () => {
        let date = expectedApplicationDate(state({
            prStartDate: new Date(2017, 0, 1),
            absencePeriods: [ {start: new Date(2018, 0, 1), end: new Date(2018, 0, 31)} ],
            temporaryResidencePeriods: [
                {start: new Date(2016, 0, 1), end: new Date(2016, 0, 20)}
            ],
        }));
        expect(date.getFullYear()).toEqual(2020);
        expect(date.getMonth()).toEqual(0);
        expect(date.getDate()).toEqual(22);  // 10 days earlier
    });

    describe('temporary residence applies now but not on expected application date', () => {
        it('temporary residence period disappears completely', () => {
            // 1 year temporary resident, 2 years out, PR status.
            // Now you need 2.5 years of PR status but the whole duration will
            // be 5.5 years so half a year of temporary residency doesn't
            // apply.  So you need more time in PR status, which invalidates
            // more temporary resident time and so on.
            let date = expectedApplicationDate(state({
                prStartDate: new Date(2017, 0, 1),
                temporaryResidencePeriods: [
                    {start: new Date(2014, 0, 1), end: new Date(2014, 11, 31)}
                ],
            }));
            expect(date.getFullYear()).toEqual(2020);
            expect(date.getMonth()).toEqual(0);
            expect(date.getDate()).toEqual(1);
        });

        it('temporary residence period disappears partially', () => {
            /*
             * 726 gives 363 => need 732 = 365 + 365 + 2,
             *   move by 2 days
             * 724 gives 362 => need 733 = 365 + 365 + 3
             *   move by 1 more
             * 723 gives 361 => need 734 = 365 + 365 + 4
             *   move by 1 more
             * 722 gives 361 => need 734 = 365 + 365 + 4
             *   no more changes.
             */
            let date = expectedApplicationDate(state({
                prStartDate: new Date(2017, 0, 1),
                temporaryResidencePeriods: [
                    {start: new Date(2014, 0, 1), end: new Date(2015, 11, 27)}
                ],
            }));
            expect(date.getFullYear()).toEqual(2019);
            expect(date.getMonth()).toEqual(0);
            expect(date.getDate()).toEqual(5);
        });
    });

    // it('part of PR time applies now but not on expected application date')
    // - not testing because in 5 years after obtaining PR you
    //   * >= 3 years in Canada and can apply for citizenship;
    //   * >= 2 years out of Canada and can lose your PR status.

    // it('have 3 years of PR status but not in the last 5 years')
    // - not testing as UI has no option to specify anything more than 5 year ago.
});
