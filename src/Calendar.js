import React, { Component } from 'react';
import './Calendar.css';
import { DateMath } from './DateMath';

/**
 * Component to display a calendar for specified time range.
 *
 * Props:
 * - startDate
 * - endDate
 * - selectable: boolean specifying if days are selectable.
 * - selection: days and ranges to highlight
 * - pendingRangeSelection: one of the dates of the pending range selection.
 * - onSelectDate: function with 1 parameter, called when a date is selected.
 *       The single argument is the selected date.
 * - className: just as regular className property.
 */
class Calendar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hoverDate: null,
            isMouseInCalendar: false,  // NOTE(vsapsai): initial value can be incorrect
        };
        this._handleDateSelect = this._handleDateSelect.bind(this);
        this._handleDateHover = this._handleDateHover.bind(this);
        this._handleMouseEnterCalendar = this._handleMouseEnterCalendar.bind(this);
        this._handleMouseLeaveCalendar = this._handleMouseLeaveCalendar.bind(this);
        this._handleElementScroll = this._handleElementScroll.bind(this);
    }

    _getAdjustedValidDate(eventDateSpec) {
        let kind = eventDateSpec.kind,
            date = eventDateSpec.date;
        let result = null;
        if (kind === 'valid') {
            result = date;
        } else if (kind === 'disabled') {
            result = this.props.startDate;
        } else {
            if (this.props.pendingRangeSelection &&
                    (DateMath.compare(this.props.pendingRangeSelection, date) < 0)) {
                // For padding choose between the first and the last day of the
                // month. Make choice so that the result is closer to
                // `pendingRangeSelection`.
                DateMath.adjustByDays(date, -1);
            }
            result = date;
            if (DateMath.compare(result, this.props.startDate) < 0) {
                result = this.props.startDate;
            }
        }
        return result;
    }

    _handleDateSelect(eventDateSpec) {
        if (!this.props.selectable) {
            return;
        }
        if ((eventDateSpec.kind === 'valid') || (this.props.pendingRangeSelection)) {
            let date = this._getAdjustedValidDate(eventDateSpec);
            if (date) {
                this.props.onSelectDate(date);
            }
        }
    }

    _handleDateHover(eventDateSpec) {
        this.setState({
            hoverDate: this._getAdjustedValidDate(eventDateSpec),
        });
    }

    _handleMouseEnterCalendar() {
        this.setState({
            isMouseInCalendar: true,
        });
    }

    _handleMouseLeaveCalendar() {
        this.setState({
            isMouseInCalendar: false,
        });
    }

    _getScrollRoot() {
        return document.getElementById('Calendar');
    }

    _handleElementScroll() {
        let scrollRoot = this._getScrollRoot();
        let yearLabels = document.getElementsByClassName('YearLabel'),
            floatingYearLabels = document.getElementsByClassName('YearLabel-floating');
        // Use regular labels for all years except for the first one above scrolling position.
        let scrollOffset = scrollRoot.scrollTop + window.pageYOffset;
        let hasFoundYearLabelAboveScroll = false;
        for (let i = yearLabels.length - 1; i >= 0; i--) {
            let yearOffset = yearLabels[i].offsetTop;
            if ((yearOffset < scrollOffset) && !hasFoundYearLabelAboveScroll) {
                // Found the year label that should float and be sticky.
                hasFoundYearLabelAboveScroll = true;
                let floatingLabel = floatingYearLabels[i];
                yearLabels[i].style.visibility = 'hidden';
                floatingLabel.style.visibility = 'visible';
                // It shouldn't overlap with the next year label, the next label
                // should push up the floating label.
                let nextLabelOverlap = 0;
                let hasNextLabel = ((i + 1) < yearLabels.length);
                if (hasNextLabel) {
                    let floatingLabelBottomOffset = scrollOffset + floatingLabel.offsetHeight;
                    let nextLabel = yearLabels[i + 1];
                    if (nextLabel.offsetTop < floatingLabelBottomOffset) {
                        nextLabelOverlap = floatingLabelBottomOffset - nextLabel.offsetTop;
                    }
                }
                if (nextLabelOverlap > 0) {
                    floatingLabel.style.top = '-' + nextLabelOverlap + 'px';
                } else {
                    floatingLabel.style.top = 0;
                }
            } else {
                yearLabels[i].style.visibility = 'visible';
                floatingYearLabels[i].style.visibility = 'hidden';
            }
        }
    }

    _syncFloatingYearLabelsWidth() {
        let yearLabels = document.getElementsByClassName('YearLabel'),
            floatingYearLabels = document.getElementsByClassName('YearLabel-floating');
        if (yearLabels.length > 0) {
            let labelWidth = yearLabels[0].clientWidth + 'px';
            let length = floatingYearLabels.length;
            for (let i = 0; i < length; i++) {
                floatingYearLabels[i].style.width = labelWidth;
            }
        }
    }

    componentDidMount() {
        let scrollRoot = this._getScrollRoot();
        if (scrollRoot) {
            scrollRoot.addEventListener('scroll', this._handleElementScroll);
        }
        window.addEventListener('scroll', this._handleElementScroll);
        this._syncFloatingYearLabelsWidth();
    }

    componentWillUnmount() {
        let scrollRoot = this._getScrollRoot();
        if (scrollRoot) {
            scrollRoot.removeEventListener('scroll', this._handleElementScroll);
        }
        window.removeEventListener('scroll', this._handleElementScroll);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props !== nextProps) {
            return true;
        }
        if (!this.props.pendingRangeSelection) {
            // Ignore state changes if there is no pending range selection.
            return false;
        }
        // NOTE(vsapsai): the following checks add code complexity and
        //     improvement isn't measured but subjectively the responsiveness
        //     with them is better then than without.
        if (this.state.isMouseInCalendar !== nextState.isMouseInCalendar) {
            return true;
        }
        if (this.state.hoverDate === nextState.hoverDate) {
            // Early exit if dates are equal by reference.
            return false;
        }
        return (!this.state.hoverDate || !nextState.hoverDate ||
            (DateMath.compare(this.state.hoverDate, nextState.hoverDate) !== 0));
    }

    render() {
        let startYear = this.props.startDate.getFullYear(),
            endYear = this.props.endDate.getFullYear();
        let rawSelection = (this.props.selection || []).slice();
        if (this.props.pendingRangeSelection) {
            // Add selection for known already selected date.
            rawSelection.push({
                type: 'single',
                className: 'pendingPeriodSelection-single',
                date: this.props.pendingRangeSelection,
            });
            // Add range selection.
            if (this.state.isMouseInCalendar && this.state.hoverDate) {
                let pendingSelection = {
                    type: 'range',
                    className: 'pendingPeriodSelection',
                    start: this.props.pendingRangeSelection,
                    end: this.state.hoverDate,
                };
                if (DateMath.compare(pendingSelection.start, pendingSelection.end) > 0) {
                    let temp = pendingSelection.start;
                    pendingSelection.start = pendingSelection.end;
                    pendingSelection.end = temp;
                }
                rawSelection.push(pendingSelection);
            }
        }
        let selectionLookup = new SelectionLookup(rawSelection);
        let yearViews = [];
        for (let year = startYear; year <= endYear; year++) {
            let quarters = [1, 2, 3, 4];
            if (year === startYear) {
                let startQuarter = getDateQuarter(this.props.startDate);
                quarters = quarters.filter(q => q >= startQuarter);
            }
            if (year === endYear) {
                let endQuarter = getDateQuarter(this.props.endDate);
                quarters = quarters.filter(q => q <= endQuarter);
            }
            yearViews.push(
                <YearView
                    key={year}
                    year={year}
                    quarters={quarters}
                    startDate={this.props.startDate}
                    selection={selectionLookup}
                    onSelectDate={this._handleDateSelect}
                    onHoverDate={this._handleDateHover}
                />
            );
        }
        let classNames = 'Calendar';
        if (this.props.selectable) {
            classNames += ' selectable';
        }
        if (this.props.className) {
            classNames += ' ' + this.props.className;
        }
        return (
            <div
                className={classNames}
                onMouseEnter={this._handleMouseEnterCalendar}
                onMouseLeave={this._handleMouseLeaveCalendar}
            >
                {yearViews}
            </div>
        );
    }
}

class YearView extends Component {
    _getEventDateSpec(event, month) {
        let dayString = event.target.getAttribute('data-day');
        let dayNumber = parseInt(dayString, 10);
        if (isNaN(dayNumber)) {
            return null;
        }
        if (dayNumber > 0) {
            let hoveredDate = new Date(this.props.year, month - 1, dayNumber);
            return {kind: 'valid', date: hoveredDate};
        }
        let unselectableKind = event.target.getAttribute('data-unselectable-kind');
        if (!unselectableKind) {
            return null;
        }
        let attributedMonth =
            (unselectableKind === 'padding-trailing') ? month : month - 1;
        let correspondingDate = new Date(this.props.year, attributedMonth, 1);
        return {kind: unselectableKind, date: correspondingDate};
    }

    _handleDayClick(month, event) {
        event.stopPropagation();
        let eventDateSpec = this._getEventDateSpec(event, month);
        if (eventDateSpec) {
            this.props.onSelectDate(eventDateSpec);
        }
    }

    _handleDayHover(month, event) {
        event.stopPropagation();
        let eventDateSpec = this._getEventDateSpec(event, month);
        if (eventDateSpec) {
            this.props.onHoverDate(eventDateSpec);
        }
    }

    render() {
        let renderedQuarters = this.props.quarters.map(q => this.renderQuarter(q));
        return (
            <div className="Year">
                <h3 className="YearLabel">{this.props.year}</h3>
                <h3 className="YearLabel-floating">{this.props.year}</h3>
                <div className="YearGrid">
                    {renderedQuarters}
                </div>
            </div>
        );
    }

    // `quarter` is 1-based.
    renderQuarter(quarter) {
        let months = [1, 2, 3].map(
            monthIndex => this.renderMonth((quarter-1) * 3 + monthIndex));
        return (
            <div key={quarter} className="Quarter">{months}</div>
        );
    }

    _isDayBeforeStartDate(date) {
        return (DateMath.compare(date, this.props.startDate) < 0);
    }

    _selectionClassNames(month, day) {
        return this.props.selection.getClassNames(this.props.year, month-1, day);
    }

    // `month` is 1-based.
    renderMonth(month) {
        const MONTH_SHORT_NAMES = [
            'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        let monthName = MONTH_SHORT_NAMES[month - 1];

        let monthDays = DateMath.getDaysInMonth(this.props.year, month-1);

        let monthStart = new Date(this.props.year, month - 1, 1);
        let startDayOfWeek = monthStart.getDay();

        let weekRows = [];
        // dayIndex represents all days, including empty days used for table padding.
        let dayIndex = 1;
        // Add padding at the beginning so the first column coresponds to Monday.
        dayIndex = dayIndex - ((startDayOfWeek + 6) % 7);
        // Create table cells for all the days.
        let temporaryDate = new Date(this.props.year, month-1, 1);
        while (dayIndex <= monthDays) {
            let rowCells = [];
            for (let i = 0; i < 7; i++) {
                let isPadding = ((dayIndex <= 0) || (monthDays < dayIndex));
                let displayedValue = isPadding ? "" : dayIndex.toString();
                if (!isPadding) {
                    temporaryDate.setDate(dayIndex);
                }
                let isDisabled = !isPadding && this._isDayBeforeStartDate(temporaryDate);
                let classNames = [];
                if (!isPadding) {
                    classNames.push(isDisabled ? 'Day-disabled' : 'Day');
                }
                classNames = classNames.concat(this._selectionClassNames(month, dayIndex));
                let unselectableKind = "";
                if (isDisabled) {
                    unselectableKind = "disabled";
                } else if (dayIndex <= 0) {
                    unselectableKind = "padding-leading";
                } else if (dayIndex > monthDays) {
                    unselectableKind = "padding-trailing";
                }
                rowCells.push(
                    <td key={dayIndex}
                        data-day={isPadding || isDisabled ? 0 : dayIndex}
                        data-unselectable-kind={unselectableKind}
                        className={classNames.join(' ')}
                    >
                        {displayedValue}
                    </td>
                );
                dayIndex++;
            }
            let rowIndex = weekRows.length;
            weekRows.push(<tr key={rowIndex}>{rowCells}</tr>);
        }
        let handleMonthDayClick = this._handleDayClick.bind(this, month);
        let handleMonthDayHover = this._handleDayHover.bind(this, month);
        return (
            <div key={month} className="Month">
                <div className="MonthLabel">{monthName}</div>
                <table
                    className="MonthGrid"
                    onClick={handleMonthDayClick}
                    onMouseMove={handleMonthDayHover}
                >
                    <tbody>
                        {weekRows}
                    </tbody>
                </table>
            </div>
        );
    }
}

class SelectionLookup {
    constructor(selectionsArray) {
        this._years = {};
        selectionsArray.forEach(selection => {
            switch (selection.type) {
                case 'single':
                    let date = selection.date;
                    this._addDate(date.getFullYear(), date.getMonth(),
                        date.getDate(), selection.className);
                    break;
                case 'range':
                    for (let date = DateMath.copy(selection.start);
                            DateMath.compare(date, selection.end) <= 0;
                            DateMath.moveToNextDay(date)) {
                        this._addDate(date.getFullYear(), date.getMonth(),
                            date.getDate(), selection.className);
                    }
                    for (let type of ['start', 'end']) {
                        let date = selection[type];
                        // start/end classes include selection class name to avoid
                        // styling start/end of one selection as another selection.
                        // For example, absence period on top of temporary residence
                        // end doesn't mean it's the end of absence period.
                        this._addDate(date.getFullYear(), date.getMonth(),
                            date.getDate(), selection.className + '-' + type);
                    }
                    break;
                default:
                    // Ignore unknown selection type.
            }
        });
    }

    _addDate(year, zbMonth, obDay, className) {
        let yearDict = this._years[year];
        if (!yearDict) {
            yearDict = {};
            this._years[year] = yearDict;
        }
        let monthDict = yearDict[zbMonth];
        if (!monthDict) {
            monthDict = {};
            yearDict[zbMonth] = monthDict;
        }
        let dayArray = monthDict[obDay];
        if (!dayArray) {
            dayArray = [];
            monthDict[obDay] = dayArray;
        }
        dayArray.push(className);
    }

    getClassNames(year, zbMonth, obDay) {
        let empty = [];
        let yearDict = this._years[year];
        if (!yearDict) {
            return empty;
        }
        let monthDict = yearDict[zbMonth];
        if (!monthDict) {
            return empty;
        }
        let dayArray = monthDict[obDay];
        if (!dayArray) {
            return empty;
        }
        return dayArray;
    }
}

// Quarters are 1-based.
function getDateQuarter(date) {
    return Math.floor(date.getMonth() / 3) + 1;
}

export default Calendar;
