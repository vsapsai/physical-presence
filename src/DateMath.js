function isLeapYear(year) {
    let divisibleBy4 = ((year % 4) === 0),
	    divisibleBy100 = ((year % 100) === 0),
	    divisibleBy400 = ((year % 400) === 0);
    return (divisibleBy400 || (divisibleBy4 && !divisibleBy100));
}

function getDaysInMonth(year, zbMonth) {
    const MONTH_DAYS = [
        31, -1, 31, 30, 31, 30,
        31, 31, 30, 31, 30, 31];
    let monthDays = MONTH_DAYS[zbMonth];
    if (zbMonth === 1) {
        monthDays = isLeapYear(year) ? 29 : 28;
    }
    return monthDays;
}

function _getDaysInYear(year) {
    return isLeapYear(year) ? 366 : 365;
}

/**
 * Compares its two arguments for order.
 *
 * Returns a negative integer, zero, or a positive integer as the first
 * argument is less than, equal to, or greater than the second.
 */
function compare(lhsDate, rhsDate) {
    let compareYear = lhsDate.getFullYear() - rhsDate.getFullYear();
    if (compareYear !== 0) {
        return compareYear;
    }
    let compareMonth = lhsDate.getMonth() - rhsDate.getMonth();
    if (compareMonth !== 0) {
        return compareMonth;
    }
    return (lhsDate.getDate() - rhsDate.getDate());
}

function adjustByDays(date, daysCount) {
    date.setDate(date.getDate() + daysCount);
}

function moveToNextDay(date) {
    adjustByDays(date, 1);
}

function copy(date) {
    return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}

/**
 * Duration calculation is inclusive, so if `from == to` the result is 1.
 */
function durationInDays(fromDate, toDate) {
    let result = _getDayNumberInYear(toDate);
    let fromYear = fromDate.getFullYear(),
        toYear = toDate.getFullYear();
    if (fromYear === toYear) {
        // fromDate defines what days to exclude.
        result = result - _getDayNumberInYear(fromDate) + 1;
    } else {
        // fromDate defines what days to include.
        result = result + (_getDaysInYear(fromYear) - _getDayNumberInYear(fromDate)) + 1;
    }
    for (let intermediateYear = fromYear + 1; intermediateYear < toYear; intermediateYear++) {
        result += _getDaysInYear(intermediateYear);
    }
    return result;
}

function _getDayNumberInYear(date) {
    let result = date.getDate();
    let year = date.getFullYear();
    for (let month = date.getMonth() - 1; month >= 0; month--) {
        result += getDaysInMonth(year, month);
    }
    return result;
}

/**
 * Duration calculation is exclusive, so if `from == to` the result is 0.
 */
function durationInNaturalUnits(fromDate, toDate) {
    let years = 0;
    let fromYear = fromDate.getFullYear(),
        toYear = toDate.getFullYear();
    if (fromYear < toYear) {
        let startDate = copy(fromDate);
        startDate.setFullYear(toYear);
        years = toYear - fromYear;
        let isLastYearComplete = (compare(startDate, toDate) <= 0);
        if (!isLastYearComplete) {
            years -= 1;
        }
    }

    let fromMonth = fromDate.getMonth(),
        toMonth = toDate.getMonth();
    if (toMonth < fromMonth) {
        toMonth += 12;
    }
    let months = toMonth - fromMonth;

    let fromDay = fromDate.getDate(),
        toDay = toDate.getDate();
    if (toDay < fromDay) {
        months -= 1;
        let monthBeforeToDate = toDate.getMonth() - 1;
        if (monthBeforeToDate < 0) {
            monthBeforeToDate += 12;
        }
        // NOTE(vsapsai): year and month can get out of sync here but it is
        //     possible only for December, so precise year doesn't matter.
        toDay += getDaysInMonth(toDate.getFullYear(), monthBeforeToDate);
    }
    let days = toDay - fromDay;
    return {
        'years': years,
        'months': months,
        'days': days,
    };
}

export const DateMath = {
    getDaysInMonth: getDaysInMonth,
    compare: compare,
    adjustByDays: adjustByDays,
    moveToNextDay: moveToNextDay,
    copy: copy,
    durationInDays: durationInDays,
    durationInNaturalUnits: durationInNaturalUnits,
};
