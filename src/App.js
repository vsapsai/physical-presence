import React, { Component } from 'react';
import './App.css';

import Calendar from './Calendar';
import DateInputField from './DateInputField';
import ImportExportModal from './ImportExportModal';
import { DateMath } from './DateMath';

const PRESENCE_PERIOD_IN_YEARS = 5;
const REQUIRED_DAYS = 1095;
const MAXIMUM_CONVERTED_TEMPORARY_RESIDENCE_DAYS = 365;

class App extends Component {
    constructor(props) {
        super(props);
        let startDate = new Date();
        startDate.setFullYear(startDate.getFullYear() - PRESENCE_PERIOD_IN_YEARS);
        this.state = {
            displayStartDate: startDate,
            prStartDate: null,
            temporaryResidencePeriods: [],
            absencePeriods: [],

            ui_selectedDateInput: new NoFocusSpec(),
            ui_importExportModalVisible: false,
            ui_explanationVisible: false,
        };
        this._activeDateInputField = null;

        this._handlePRStartDateFocus = this._handlePRStartDateFocus.bind(this);
        this._handleNonDateInputFieldFocus = this._handleNonDateInputFieldFocus.bind(this);
        this._deselectElements = this._deselectElements.bind(this);
        this._handleSelectDate = this._handleSelectDate.bind(this);
        this._handleEscapeKey = this._handleEscapeKey.bind(this);
        this._handleImportShow = this._handleImportShow.bind(this);
        this._handleImportConfirm = this._handleImportConfirm.bind(this);
        this._handleImportCancel = this._handleImportCancel.bind(this);
        this._setActiveDateInputField = this._setActiveDateInputField.bind(this);
    }

    _handlePRStartDateFocus() {
        this.setState({
            ui_selectedDateInput: new FocusedDateInputSpec('prStartDate'),
        });
    }

    _handleTimePeriodDateFocus(periodsName, type, index) {
        assert(0 <= index && index < this.state[periodsName].length,
            'Period for edit field in focus does not exist');
        this.setState({
            ui_selectedDateInput: new FocusedDateInputSpec(periodsName, type, index),
        });
    }

    _handleNonDateInputFieldFocus() {
        // If another element has a visible focus, need to remove our custom
        // focus for date input field. Cannot use onBlur event because it is
        // triggered on clicks in the calendar and we want to know which field
        // is selected during those clicks.
        this.setState({
            ui_selectedDateInput: new NoFocusSpec(),
        });
    }

    _deselectElements() {
        // Need to call `blur` explicitly because deselecting with keyboard
        // will remove only focused-state styling but not actual focus and
        // clicking in the element again won't trigger `onFocus` because it is
        // already in focus.
        document.activeElement.blur();
        this.setState((prevState) => {
            let newState = {
                ui_selectedDateInput: new NoFocusSpec(),
            };
            let focusSpec = prevState.ui_selectedDateInput;
            if (focusSpec.isPeriod()) {
                let allPeriods = focusSpec.getAllPeriodsReference(prevState);
                if (allPeriods.length > 1) {
                    // Don't modify prevState.
                    allPeriods = allPeriods.slice();
                    allPeriods.sort(_periodsComparator);
                    newState[focusSpec.target] = allPeriods;
                }
            }
            return newState;
        });
    }

    _setActiveDateInputField(element, isActive) {
        if (isActive) {
            this._activeDateInputField = element;
        }
    }

    _handleSelectDate(date) {
        this.setState((prevState) => {
            let focusSpec = prevState.ui_selectedDateInput;
            let newState = focusSpec.stateBySettingDate(prevState, date);
            let newFocus = null;
            if (focusSpec.isPeriod()) {
                // Order start and end dates.
                let period = focusSpec.getPeriodReference(newState);
                if (period.start && period.end) {
                    if (DateMath.compare(period.start, period.end) > 0) {
                        let temporary = period.start;
                        period.start = period.end;
                        period.end = temporary;
                    }
                } else {
                    // Auto-select another field if it's empty.
                    let otherComponentFocus = focusSpec.otherPeriodComponentFocus();
                    if (!otherComponentFocus.getValue(newState)) {
                        newFocus = otherComponentFocus;
                    }
                }
                if (!newFocus) {
                    // Sort periods but don't move anything if there is still focus.
                    let allPeriods = focusSpec.getAllPeriodsReference(newState);
                    allPeriods.sort(_periodsComparator);
                }
            }
            newState.ui_selectedDateInput = newFocus || new NoFocusSpec();
            return newState;
        });
    }

    _handleEscapeKey(event) {
        if (event.keyCode === 27) {
            this._deselectElements();
            this._handleImportCancel();
        }
    }

    _addTimePeriod(periodsName, event) {
        event.stopPropagation();
        this.setState(prevState => {
            let emptyPeriod = {
                start: null,
                end: null,
            };
            let newPeriods = prevState[periodsName].slice();
            newPeriods.push(emptyPeriod);
            let newState = {
                ui_selectedDateInput: new FocusedDateInputSpec(
                    periodsName, 'start', newPeriods.length - 1),
            };
            newState[periodsName] = newPeriods;
            return newState;
        });
    }

    _removeTimePeriod(periodsName, index, event) {
        event.stopPropagation();
        this.setState(prevState => {
            let periods = prevState[periodsName].slice();
            assert(0 <= index && index < periods.length, 'Period index is out of range');
            periods.splice(index, 1);
            let newState = {};
            newState[periodsName] = periods;
            let newFocus = prevState.ui_selectedDateInput.focusAfterRemoving(
                periodsName, index);
            if (newFocus !== prevState.ui_selectedDateInput) {
                newState.ui_selectedDateInput = newFocus;
            }
            return newState;
        });
    }

    _handleImportShow() {
        this.setState({
            ui_importExportModalVisible: true,
        });
    }

    _exportData(state) {
        let exportObject = {};
        if (state.prStartDate) {
            exportObject['permanent_resident'] = formatDateISO(state.prStartDate);
        }
        function exportPeriods(periods) {
            return periods.filter(period => {
                return period.start && period.end;
            })
            .map(period => {
                return {
                    start: formatDateISO(period.start),
                    end: formatDateISO(period.end),
                };
            });
        }
        exportObject['temporary_resident'] = exportPeriods(state.temporaryResidencePeriods);
        exportObject['absent'] = exportPeriods(state.absencePeriods);
        return JSON.stringify(exportObject, null, /*indent=*/4);
    }

    _handleImportConfirm(importedContent) {
        let importObject;
        try {
            importObject = JSON.parse(importedContent);
        } catch(error) {
            return 'Input is not valid JSON.';
        }
        let newState = {
            ui_importExportModalVisible: false,
        };
        // Validate data and transform it into state-compatible format.
        let key = 'permanent_resident';
        let value = importObject[key];
        if (value) {
            let {isValid, date} = parseDateISO(value);
            if (isValid) {
                newState.prStartDate = date;
            } else {
                return "'" + key + "' value '" + value + "' is not a valid date.";
            }
        } else {
            newState.prStartDate = null;
        }
        for (let [importKey, stateKey] of [
                ['temporary_resident', 'temporaryResidencePeriods'],
                ['absent', 'absencePeriods']]) {
            key = importKey;
            value = importObject[key];
            if ((value === undefined) || (value === null)) {
                newState[stateKey] = [];
            } else {
                if (!Array.isArray(value)) {
                    return "'" + key + "' should be an array.";
                }
                let periods = [];
                for (let i = 0, length = value.length; i < length; i++) {
                    let periodValue = value[i];
                    if ((typeof(periodValue) !== 'object') || !periodValue.start || !periodValue.end) {
                        return "All '" + key + "' values should be objects with 'start' and 'end' values.";
                    }
                    let period = {};
                    for (let dateKind of ['start', 'end']) {
                        let dateValue = periodValue[dateKind];
                        let {isValid, date} = parseDateISO(dateValue);
                        if (isValid) {
                            period[dateKind] = date;
                        } else {
                            return "'" + key + "' value '" + dateValue + "' is not a valid date.";
                        }
                    }
                    periods.push(period);
                }
                newState[stateKey] = periods;
            }
        }
        // Sort start/end and periods themselves.
        for (let stateKey of ['temporaryResidencePeriods', 'absencePeriods']) {
            let periods = newState[stateKey];
            periods.forEach(period => {
                if (DateMath.compare(period.start, period.end) > 0) {
                    let temporary = period.start;
                    period.start = period.end;
                    period.end = temporary;
                }
            });
            periods.sort(_periodsComparator);
        }
        this.setState(newState);
    }

    _handleImportCancel() {
        this.setState({
            ui_importExportModalVisible: false,
        });
    }

    _handleExplanationVisibility(isVisible) {
        this.setState({
            ui_explanationVisible: isVisible,
        });
    }

    _handleSizeChange() {
        let windowHeight = window.innerHeight;
        let descriptionColumn = document.getElementById('MinSizeDescription');
        let calendarColumn = document.getElementById('Calendar');
        if (!descriptionColumn || !calendarColumn) {
            return;
        }
        let desiredHeight = Math.max(windowHeight, descriptionColumn.scrollHeight);
        calendarColumn.style.height = desiredHeight + 'px';
    }

    componentDidMount() {
        window.addEventListener('resize', this._handleSizeChange);
        window.addEventListener('keyup', this._handleEscapeKey);
        // Invoke first time manually.
        this._handleSizeChange();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this._handleSizeChange);
        window.removeEventListener('keyup', this._handleEscapeKey);
    }

    componentDidUpdate() {
        // If we re-render due to adding/removing a time period, the size changes.
        this._handleSizeChange();
        // Set browser focus for active date input field.
        if (this._activeDateInputField) {
            this._activeDateInputField.focus();
            this._activeDateInputField = null;
        }
    }

    _validatePeriods(periodsName, periods, focusSpec, prDate) {
        let result = {};
        // Verify incomplete periods.
        periods.forEach((period, index) => {
            let isExactlySingleComponentSpecified = (
                (period.start || period.end) && !(period.start && period.end));
            if (isExactlySingleComponentSpecified &&
                    !focusSpec.hasPeriodFocus(periodsName, index)) {
                result[index] = 'Incomplete periods are ignored';
            }
        });
        // Verify overlapping periods.
        var lastPeriod = null;
        periods.forEach((period, index) => {
            if (!period.start || !period.end) {
                // Skip incomplete periods. Not filtering them out to preserve indices.
                return;
            }
            if (!lastPeriod) {
                lastPeriod = period;
                return;
            }
            if (DateMath.compare(period.start, lastPeriod.end) <= 0) {
                result[index] = 'Overlapping periods are counted only once';
            }
            // New last period is the one which ends later.
            if (DateMath.compare(period.end, lastPeriod.end) > 0) {
                lastPeriod = period;
            }
        });
        // Verify temporary residence after PR.
        if (prDate && (periodsName === 'temporaryResidencePeriods')) {
            periods.forEach((period, index) => {
                if (!period.start || !period.end) {
                    return;
                }
                if (DateMath.compare(period.end, prDate) >= 0) {
                    result[index] = 'Temporary residence after obtaining PR status is ignored';
                }
            });
        }
        return result;
    }

    renderTimePeriodTable(periodsName) {
        let periods = this.state[periodsName] || [];
        let focusSpec = this.state.ui_selectedDateInput;
        let validationMessages = this._validatePeriods(
            periodsName, periods, focusSpec, this.state.prStartDate);
        let addPeriodHandler = this._addTimePeriod.bind(this, periodsName);
        let periodRows = periods.map((period, index) => {
            let startFieldFocusHandler = this._handleTimePeriodDateFocus.bind(
                this, periodsName, 'start', index);
            let endFieldFocusHandler = this._handleTimePeriodDateFocus.bind(
                this, periodsName, 'end', index);
            let isStartFieldSelected = focusSpec.isFocused(periodsName, 'start', index);
            let isEndFieldSelected = focusSpec.isFocused(periodsName, 'end', index);
            let removePeriodHandler = this._removeTimePeriod.bind(this, periodsName, index);
            let warningElement = null;
            let warning = validationMessages[index];
            if (warning) {
                warningElement = (<span><br />
                    <span className='Warning'>{warning}</span>
                </span>);
            }
            return (<li key={index} className="PeriodRow">
                <span className="Cell">
                    <DateInputField
                        value={formatDate(period.start)}
                        selected={isStartFieldSelected}
                        onFocus={startFieldFocusHandler}
                        ref={el => this._setActiveDateInputField(el, isStartFieldSelected) }
                    />
                </span>
                <span className="Cell">
                    <DateInputField
                        value={formatDate(period.end)}
                        selected={isEndFieldSelected}
                        onFocus={endFieldFocusHandler}
                        ref={el => this._setActiveDateInputField(el, isEndFieldSelected) }
                    />
                </span>
                <button
                    className="LinkButton IconButton"
                    onClick={removePeriodHandler}
                    onFocus={this._handleNonDateInputFieldFocus}
                >&#x2715;</button>
                {warningElement}
            </li>);
        });

        let headerRow = null;
        if (periods.length > 0) {
            headerRow = (<li>
                <span className="Header">Start</span>
                <span className="Header">End</span>
            </li>);
        }

        return (
            <ul className="PeriodsList">
                {headerRow}
                {periodRows}
                <li className="ControlRow">
                    <button
                        className="LinkButton TableButton"
                        onClick={addPeriodHandler}
                        onFocus={this._handleNonDateInputFieldFocus}
                    >Add a period</button>
                </li>
            </ul>
        );
    }

    render() {
        let endDate = new Date();
        let selections = [];
        if (this.state.prStartDate) {
            selections.push({
                type: 'single',
                date: this.state.prStartDate,
                className: 'prStartDate-selected',
            });
        }
        ['temporaryResidencePeriods', 'absencePeriods'].forEach(periodName => {
            this.state[periodName].forEach(period => {
                if (period.start && period.end) {
                    selections.push({
                        type: 'range',
                        start: period.start,
                        end: period.end,
                        className: periodName + '-selected',
                    });
                }
            });
        });
        let focusSpec = this.state.ui_selectedDateInput;
        let isPRStartDateFocused = focusSpec.isFocused('prStartDate');

        let pendingPeriodSelection = null;
        if (focusSpec.isPeriod() && !focusSpec.getValue(this.state)) {
            // Pending period selection means:
            // * one period component is selected and has no value;
            // * another period component has a value.
            let otherPeriodFocus = focusSpec.otherPeriodComponentFocus();
            pendingPeriodSelection = otherPeriodFocus.getValue(this.state);
        }

        let pendingSelectionClassName = '';
        if (!focusSpec.isEmpty()) {
            pendingSelectionClassName = focusSpec.target + '-pendingSelection';
        }

        let exportContent = '';
        if (this.state.ui_importExportModalVisible) {
            exportContent = this._exportData(this.state);
        }

        let resultSection = null;
        let state = canonicalState(this.state);
        let applicationDate = expectedApplicationDate(state);
        if (applicationDate) {
            // NOTE(vsapsai): or maybe endDate should be maximum of the
            //     application date and now?
            endDate = applicationDate;
            selections.push({
                type: 'single',
                date: applicationDate,
                className: 'applicationDate-selected',
            });
            let prSelectionStartDate = DateMath.copy(this.state.prStartDate),
                prSelectionEndDate = DateMath.copy(applicationDate);
            DateMath.adjustByDays(prSelectionStartDate, +1);
            DateMath.adjustByDays(prSelectionEndDate, -1);
            selections.push({
                type: 'range',
                start: prSelectionStartDate,
                end: prSelectionEndDate,
                className: 'permanentResidencePeriods-selected',
            });
            let shortSummaryText = _applicationDateSummary(applicationDate);
            let explanationClassName =
                this.state.ui_explanationVisible ? 'Explanation-visible' : '';
            let showExplanationFn = this._handleExplanationVisibility.bind(this, true),
                hideExplanationFn = this._handleExplanationVisibility.bind(this, false);
            let explanationData = _applicationDateExplanation(applicationDate, state);
            resultSection = (
                <div className={explanationClassName}>
                    <div className="Short">
                        {shortSummaryText}
                        <button
                            className="LinkButton"
                            onClick={showExplanationFn}
                            onFocus={this._handleNonDateInputFieldFocus}
                        >
                            Show explanation
                        </button>
                    </div>
                    <div className="Long">
                        <ResultsExplanation data={explanationData} />
                        <button
                            className="LinkButton"
                            onClick={hideExplanationFn}
                            onFocus={this._handleNonDateInputFieldFocus}
                        >
                            Hide explanation
                        </button>
                    </div>
                </div>
            );
        } else {
            resultSection = (
                <div>
                    To apply for the citizenship you need to obtain the permanent resident status.
                </div>
            );
        }
        return (
            <div className="App" onClick={this._deselectElements}>
                <ImportExportModal
                    visible={this.state.ui_importExportModalVisible}
                    content={exportContent}
                    onImport={this._handleImportConfirm}
                    onCancel={this._handleImportCancel}
                />
                <div className="MultiColumnPanel">
                    <div className="Column" id="Description">
                      <div id="MinSizeDescription">
                        <div>
                            This is the alternative version of the Physical
            Presence Calculator for permanent residents applying for Canadian
            citizenship. It is more interactive but it might be outdated and
            not reflecting the latest regulations. For the citizenship
            application please use <a
            href="https://eservices.cic.gc.ca/rescalc/resCalcStartNew.do"
            target="_blank" rel="noopener noreferrer"
            onFocus={this._handleNonDateInputFieldFocus}>the official
            calculator</a>. The calculator implements the physical presence
            rules as of June&nbsp;1st, 2018.
                        </div>
                        <div>
                            <h3>Permanent resident</h3>
Enter the date you became a permanent resident of Canada (see Box&nbsp;45 on your Record of Landing (IMM&nbsp;1000), Box&nbsp;46 on your Confirmation of Permanent Residence (IMM&nbsp;5292 or IMM&nbsp;5509) or the back of your Permanent Resident Card).

                            <div className="DateInputArea">
                                <DateInputField
                                    value={formatDate(this.state.prStartDate)}
                                    selected={isPRStartDateFocused}
                                    onFocus={this._handlePRStartDateFocus}
                                    ref={el => this._setActiveDateInputField(el, isPRStartDateFocused) }
                                />
                            </div>
                        </div>

                        <div>
                            <h3>Temporary resident</h3>
Did you have Canadian temporary resident status (i.e. as a visitor, international student, temporary worker, temporary resident permit holder), and/or were a protected person in the last 5 years?
Please only list the periods of time you had authorized temporary resident or protected person status in Canada.
                            {this.renderTimePeriodTable('temporaryResidencePeriods')}
                        </div>

                        <div>
                            <h3>Periods of absence</h3>
Did you leave Canada in the last 5 years? Please list the periods of time you left Canada for any reason including vacation, work, business, family matters, school, illness, etc.<br />
Please list the periods of time you spent serving a sentence for an offence in Canada (e.g. "term of imprisonment", conditional sentence, probation and or parole).
                            {this.renderTimePeriodTable('absencePeriods')}
                        </div>

                        <div className="Result">
                            {resultSection}
                        </div>

                        <div>
                            <button
                                className="Button"
                                onClick={this._handleImportShow}
                                onFocus={this._handleNonDateInputFieldFocus}
                            >Import/Export</button>
                        </div>
                      </div>
                    </div>
                    <div className="Column" id="Calendar">
                        <Calendar
                            className={pendingSelectionClassName}
                            startDate={this.state.displayStartDate}
                            endDate={endDate}
                            selectable={!this.state.ui_selectedDateInput.isEmpty()}
                            selection={selections}
                            pendingRangeSelection={pendingPeriodSelection}
                            onSelectDate={this._handleSelectDate}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

const COLUMNS_COUNT = 5;

/**
 * Component to display a detailed explanation of the application date calculation.
 *
 * Props:
 * - data
 */
class ResultsExplanation extends Component {

    _renderRow(text, value, valueColumn, cellStyles) {
        assert((0 <= valueColumn) && (valueColumn < (COLUMNS_COUNT - 1)),
            'Incorrect column for a value');
        let styles = cellStyles || {};
        let textCellStyle = styles['textCell'] || '',
            summaryCellStyle = styles['summaryCell'] || '';
        let cells = [<td key={COLUMNS_COUNT-1} className={textCellStyle}>{text}</td>];
        for (let i = COLUMNS_COUNT - 2; i >= 0; i--) {
            let cellStyle = '';
            if ((i === valueColumn) || (i-1 === valueColumn)) {
                cellStyle += ' ' + summaryCellStyle;
            }
            if (i === valueColumn) {
                cells.push(<td key={i} className={cellStyle}>{value}</td>);
            } else {
                cells.push(<td key={i} className={cellStyle}></td>);
            }
        }
        let rowKey = this.rowIndex++;
        return (<tr key={rowKey} className="Row">{cells}</tr>);
    }

    render() {
        this.rowIndex = 0;
        let rows = [];
        let totalDays = 0;
        rows.push(this._renderRow('Required days of physical presence',
            this.props.data.requiredDays, 1));
        totalDays += this.props.data.requiredDays;
        if (this.props.data.temporaryResidencePeriods.length > 0) {
            rows.push(this._renderRow('You were a temporary resident', '', 3));
            let temporaryResidenceDays = 0;
            let isFirstPeriod = true;
            this.props.data.temporaryResidencePeriods.forEach(period => {
                let duration = DateMath.durationInDays(period.start, period.end);
                let displayValue = isFirstPeriod ? duration : ('+' + duration);
                temporaryResidenceDays += duration;
                rows.push(this._renderRow(
                    `from ${formatDate(period.start)} till ${formatDate(period.end)}`,
                    displayValue, 3, {textCell: 'Indent-2'}));
                isFirstPeriod = false;
            });
            if (this.props.data.temporaryResidenceAbsencePeriods.length > 0) {
                rows.push(this._renderRow('excluding absence', '', 3, {textCell: 'Indent-1'}));
                this.props.data.temporaryResidenceAbsencePeriods.forEach(period => {
                    let duration = DateMath.durationInDays(period.start, period.end);
                    temporaryResidenceDays -= duration;
                    rows.push(this._renderRow(
                        `from ${formatDate(period.start)} till ${formatDate(period.end)}`,
                        negative(duration), 3, {textCell: 'Indent-2'}));
                });
            }
            if (this.props.data.ignoredTemporaryResidencePeriods.length > 0) {
                rows.push(this._renderRow('ignoring days 5 years before application date', '', 3, {textCell: 'Indent-1'}));
                this.props.data.ignoredTemporaryResidencePeriods.forEach(period => {
                    rows.push(this._renderRow(
                        `from ${formatDate(period.start)} till ${formatDate(period.end)}`,
                        '', 3));
                });
            }
            rows.push(this._renderRow('', temporaryResidenceDays, 2, {summaryCell: 'LinedSummary'}));
            temporaryResidenceDays = this.props.data.adjustedTemporaryResidenceDays;
            rows.push(this._renderRow('after counting 2 days for 1', temporaryResidenceDays, 2, {textCell: 'Indent-1'}));
            if (this.props.data.cappedTemporaryResidenceDays) {
                temporaryResidenceDays = this.props.data.cappedTemporaryResidenceDays;
                rows.push(this._renderRow(`capped at ${MAXIMUM_CONVERTED_TEMPORARY_RESIDENCE_DAYS} days`,
                    temporaryResidenceDays, 2, {textCell: 'Indent-1'}));
            }
            rows.push(this._renderRow('', negative(temporaryResidenceDays), 1, {summaryCell: 'LinedSummary'}));
            totalDays -= temporaryResidenceDays;
        }
        if (this.props.data.permanentResidenceDays > 0) {
            rows.push(this._renderRow('You were a permanent resident', '', 3));
            let prDays = this.props.data.permanentResidenceDays;
            rows.push(this._renderRow(`from ${formatDate(this.props.data.permanentResidenceStartDate)} till now`,
                prDays, 2, {textCell: 'Indent-2'}));
            if (this.props.data.permanentResidenceAbsencePeriods.length > 0) {
                rows.push(this._renderRow('excluding absence', '', 3, {textCell: 'Indent-1'}));
                this.props.data.permanentResidenceAbsencePeriods.forEach(period => {
                    let duration = DateMath.durationInDays(period.start, period.end);
                    prDays -= duration;
                    rows.push(this._renderRow(
                        `from ${formatDate(period.start)} till ${formatDate(period.end)}`,
                        negative(duration), 2, {textCell: 'Indent-2'}));
                });
            }
            rows.push(this._renderRow('', negative(prDays), 1, {summaryCell: 'LinedSummary'}));
            totalDays -= prDays;
        }
        if (this.props.data.futurePermanentResidenceAbsencePeriods.length > 0) {
            rows.push(this._renderRow('You expect to be absent', '', 3));
            let futureAbsenceDays = 0;
            let isFirstPeriod = true;
            this.props.data.futurePermanentResidenceAbsencePeriods.forEach(period => {
                let duration = DateMath.durationInDays(period.start, period.end);
                let displayValue = isFirstPeriod ? duration : ('+' + duration);
                futureAbsenceDays += duration;
                rows.push(this._renderRow(
                    `from ${formatDate(period.start)} till ${formatDate(period.end)}`,
                    displayValue, 2, {textCell: 'Indent-2'}));
                isFirstPeriod = false;
            });
            rows.push(this._renderRow('', '+' + futureAbsenceDays, 1, {summaryCell: 'LinedSummary'}));
            totalDays += futureAbsenceDays;
        }
        rows.push(this._renderRow('Remaining days of required physical presence', prettyIfNegative(totalDays), 0, {summaryCell: 'LinedSummary BoldSummary'}));
        if (totalDays > 0) {
            let now = new Date();
            let remainingDays = DateMath.durationInNaturalUnits(now, this.props.data.applicationDate);
            let textComponents = [];
            for (let [plural, singular] of
                    [['years', 'year'], ['months', 'month'], ['days', 'day']]) {
                let value = remainingDays[plural];
                if (value > 0) {
                    let unit = (value > 1) ? plural : singular;
                    textComponents.push(`${value} ${unit}`);
                }
            }
            let cellText = textComponents.join(' ');
            // Here is an extremely elaborate hack. Text in this cell can be
            // fairly long and the problem is that affects the length of the
            // border in the previous row. Too long border is undesirable and
            // that's why text is wrapped in FloatingValue so it doesn't
            // participate in page layout. But we want to avoid only horizontal
            // contribution to the layout, not the vertical because otherwise
            // the text after the table would overlap with this row. And the
            // unbreaking space is there to keep some vertical space.
            let cell = (<span><span className="FloatingValue">{cellText}</span>&nbsp;</span>);
            rows.push(this._renderRow('', cell, 0));
        }
        let shortSummaryText = _applicationDateSummary(this.props.data.applicationDate);
        return (
            <div>
                <table>
                    <tbody>
                        {rows}
                    </tbody>
                </table>
                {shortSummaryText}
            </div>
        );
    }
}

function negative(number) {
    return '−' + number;
}

function prettyIfNegative(number) {
    return (number >= 0) ? number : negative(-number);
}

class FocusedDateInputSpec {
    constructor(target, type, index) {
        this.target = target;
        this.type = type;
        this.index = index;
        if (type) {
            assert(type === 'start' || type === 'end',
                'Unknown period component "' + type + '".');
        }
    }

    isFocused(target, type, index) {
        if (this.target !== target) {
            return false;
        }
        if (this.type !== type) {
            return false;
        }
        if (this.index !== index) {
            return false;
        }
        return true;
    }

    isEmpty() {
        return false;
    }

    stateBySettingDate(prevState, date) {
        let result = {};
        if (this.type) {
            // Nested structure.
            let prevDates = prevState[this.target].slice();
            assert(0 <= this.index && this.index < prevDates.length, 'Invalid focused field index.');
            prevDates[this.index] = Object.assign({}, prevDates[this.index]);
            prevDates[this.index][this.type] = date;
            result[this.target] = prevDates;
        } else {
            // Flat structure.
            result[this.target] = date;
        }
        return result;
    }

    getValue(state) {
        if (this.isPeriod()) {
            return this.getPeriodReference(state)[this.type];
        }
        return state[this.target];
    }

    isPeriod() {
        return !!this.type;
    }

    getPeriodReference(state) {
        assert(this.isPeriod(), 'Support slicing only for periods.');
        return state[this.target][this.index];
    }

    getAllPeriodsReference(state) {
        assert(this.isPeriod(), 'Support slicing only for periods.');
        return state[this.target];
    }

    otherPeriodComponentFocus() {
        assert(this.isPeriod(), 'Supported only for periods.');
        let otherType = (this.type === 'start') ? 'end' : 'start';
        return new FocusedDateInputSpec(this.target, otherType, this.index);
    }

    focusAfterRemoving(target, index) {
        if ((target !== this.target) || (index > this.index)) {
            return this;
        }
        return (index === this.index) ? new NoFocusSpec()
            : new FocusedDateInputSpec(this.target, this.type, this.index - 1);
    }

    hasPeriodFocus(target, index) {
        return (this.isPeriod() && (target === this.target) && (index === this.index));
    }
}

class NoFocusSpec {
    isFocused() {
        return false;
    }

    isEmpty() {
        return true;
    }

    stateBySettingDate(prevState, date) {
        assert(false, 'Cannot set date as no input field is selected.');
    }

    getValue(state) {
        assert(false, 'Cannot get value as no input field is selected.');
    }

    isPeriod() {
        return false;
    }

    getPeriodReference(state) {
        assert(false, 'Cannot get reference as no input field is selected.');
    }

    getAllPeriodsReference(state) {
        assert(false, 'Cannot get reference as no input field is selected.');
    }

    focusAfterRemoving(target, index) {
        return this;
    }

    hasPeriodFocus(target, index) {
        return false;
    }
}

class AssertionError {
    constructor(message) {
        this.message = message;
    }
}

function assert(condition, message) {
    if (!condition) {
        throw new AssertionError(message);
    }
}

function _zeroPadding(number) {
    return ((number >= 10) ? '' : '0') + number;
}
function formatDate(date) {
    if (!date) {
        return '';
    }
    return date.getFullYear() +
        '-' + _zeroPadding(date.getMonth() + 1) +
        '-' + _zeroPadding(date.getDate());
}
function formatDateISO(date) {
    return formatDate(date);
}

function parseDateISO(value) {
    let invalidResult = {isValid: false, date: null};
    if (typeof(value) !== 'string') {
        return invalidResult;
    }
    let dateRegExp = RegExp('^\\d{4}-\\d{2}-\\d{2}$');
    if (!dateRegExp.test(value)) {
        return invalidResult;
    }
    let components = value.split('-');
    assert(components.length === 3, 'Internal inconsistency: split and regular expression');
    let year = parseInt(components[0], 10),
        month = parseInt(components[1], 10),
        day = parseInt(components[2], 10);
    if ((month < 0) || (12 <= month)) {
        return invalidResult;
    }
    let zbMonth = month - 1;
    if ((day < 1) || (DateMath.getDaysInMonth(year, zbMonth) < day)) {
        return invalidResult;
    }
    return {isValid: true, date: new Date(year, zbMonth, day)};
}

function _periodsComparator(lhs, rhs) {
    let lhsDate = lhs.start || lhs.end,
        rhsDate = rhs.start || rhs.end;
    if (!lhsDate && !rhsDate) {
        return 0;
    }
    if (!lhsDate && rhsDate) {
        return 1;
    }
    if (lhsDate && !rhsDate) {
        return -1;
    }
    let comparison = DateMath.compare(lhsDate, rhsDate);
    if (comparison !== 0) {
        return comparison;
    }
    // Primary period dates are equal.
    let isLhsComplete = lhs.start && lhs.end,
        isRhsComplete = rhs.start && rhs.end;
    if (!isLhsComplete && !isRhsComplete) {
        return 0;
    }
    if (!isLhsComplete && isRhsComplete) {
        return 1;
    }
    if (isLhsComplete && !isRhsComplete) {
        return -1;
    }
    // Both periods are complete and start at the same day,
    // compare end dates.
    return DateMath.compare(lhs.end, rhs.end);
}

/**
 * Returns new, canonicalized state.
 */
function canonicalState(state) {
    let result = {};
    result.prStartDate = state.prStartDate;
    result.absencePeriods = _canonicalPeriods(state.absencePeriods);
    result.effectiveTemporaryResidenceAbsencePeriods = [];
    let temporaryResidencePeriods = _canonicalPeriods(state.temporaryResidencePeriods);

    // Apply absences to the temporary residence periods.
    let cleanTemporaryResidencePeriods = [];
    let trIndex = 0, absenceIndex = 0;
    while ((trIndex < temporaryResidencePeriods.length) &&
            (absenceIndex < result.absencePeriods.length)) {
        let trPeriod = temporaryResidencePeriods[trIndex],
            absencePeriod = result.absencePeriods[absenceIndex];
        if (DateMath.compare(trPeriod.end, absencePeriod.start) < 0) {
            // TR period ends before the absence begins.
            cleanTemporaryResidencePeriods.push(trPeriod);
            trIndex++;
            continue;
        }
        if (DateMath.compare(absencePeriod.end, trPeriod.start) < 0) {
            // Absence period ends before the TR period starts.
            absenceIndex++;
            continue;
        }
        // There is an overlap between trPeriod and absencePeriod.
        // For effective TR absence you need to take the latest of start dates,
        // the earliest of end dates.
        let effectiveTRAbsencePeriod = {
            start: (DateMath.compare(trPeriod.start, absencePeriod.start) < 0)
                ? absencePeriod.start : trPeriod.start,
            end: (DateMath.compare(trPeriod.end, absencePeriod.end) < 0)
                ? trPeriod.end : absencePeriod.end,
        };
        result.effectiveTemporaryResidenceAbsencePeriods.push(effectiveTRAbsencePeriod);
        // Calculate truncated temporary residence period.
        if (DateMath.compare(trPeriod.start, absencePeriod.start) < 0) {
            // TR period head is available.
            let period = { start: trPeriod.start, end: DateMath.copy(absencePeriod.start) };
            DateMath.adjustByDays(period.end, -1);
            cleanTemporaryResidencePeriods.push(period);
        }
        if (DateMath.compare(trPeriod.end, absencePeriod.end) <= 0) {
            // ..and the tail entirely not available.
            trIndex++;
        } else {
            // ..and the tail maybe not available. Need to check with the next
            // absence period.
            trPeriod.start = DateMath.copy(absencePeriod.end);
            DateMath.adjustByDays(trPeriod.start, 1);
            // Will check this TR period with the next absence period.
            absenceIndex++;
        }
    }
    // Add remaining temporary residence periods without checking absence periods.
    while (trIndex < temporaryResidencePeriods.length) {
        cleanTemporaryResidencePeriods.push(temporaryResidencePeriods[trIndex]);
        trIndex++;
    }
    // Keep temporary residence periods only before PR start date if it is present.
    let originalTemporaryResidencePeriods = _canonicalPeriods(state.temporaryResidencePeriods);
    if (result.prStartDate) {
        cleanTemporaryResidencePeriods = _keepPeriodsBeforeDate(
            cleanTemporaryResidencePeriods, result.prStartDate);
        originalTemporaryResidencePeriods = _keepPeriodsBeforeDate(
            originalTemporaryResidencePeriods, result.prStartDate);
    }
    result.temporaryResidencePeriods = cleanTemporaryResidencePeriods;
    result.originalTemporaryResidencePeriods = originalTemporaryResidencePeriods;

    // Keep absence periods only after PR start date.
    if (result.prStartDate) {
        result.absencePeriods = _keepPeriodsAfterDate(
            result.absencePeriods, result.prStartDate);
    } else {
        result.absencePeriods = [];
    }
    return result;
}

// Excluding the `date`.
function _keepPeriodsBeforeDate(periods, date) {
    let result = periods.filter(period => {
        return (DateMath.compare(period.start, date) < 0);
    });
    let lastPeriod = null;
    if (result.length > 0) {
        lastPeriod = result[result.length - 1];
    }
    if (lastPeriod && (DateMath.compare(lastPeriod.end, date) >= 0)) {
        lastPeriod = Object.assign({}, lastPeriod);
        lastPeriod.end = DateMath.copy(date);
        DateMath.adjustByDays(lastPeriod.end, -1);
        result[result.length - 1] = lastPeriod;
    }
    return result;
}
// Including the `date`.
function _keepPeriodsAfterDate(periods, date) {
    let result = periods.filter(period => {
        return (DateMath.compare(period.end, date) >= 0);
    });
    if ((result.length > 0) && (DateMath.compare(result[0].start, date) < 0)) {
        result[0] = Object.assign({}, result[0]);
        result[0].start = DateMath.copy(date);
    }
    return result;
}

function _canonicalPeriods(periods) {
    let validPeriods = periods.filter(period => {
        return (period.start && period.end);
    });
    validPeriods = validPeriods.map(period => {
        return {
            start: DateMath.copy(period.start),
            end: DateMath.copy(period.end),
        };
    });
    // Flatten all overlapping periods.
    let flatPeriods = [];
    if (validPeriods.length > 0) {
        let lastPeriod = validPeriods[0];
        flatPeriods.push(lastPeriod);
        for (let i = 1; i < validPeriods.length; i++) {
            let candidatePeriod = validPeriods[i];
            if (DateMath.compare(candidatePeriod.end, lastPeriod.end) <= 0) {
                // Next period ends before the current one.
                continue;
            }
            if (DateMath.compare(candidatePeriod.start, lastPeriod.end) <= 0) {
                // Next period starts before the current one ends.
                candidatePeriod.start = DateMath.copy(lastPeriod.end);
                DateMath.moveToNextDay(candidatePeriod.start);
            }
            lastPeriod = candidatePeriod;
            flatPeriods.push(lastPeriod);
        }
    }
    return flatPeriods;
}

/**
 * Returns the date when physical presence requirements will be satisfied.
 *
 * Input state should be canonicalized:
 * - no invalid periods;
 * - absence periods are only for permanent resident status;
 * - periods are ordered;
 * - with no overlapping periods.
 */
function expectedApplicationDate(state) {
    if (!state.prStartDate) {
        return null;
    }
    let effectiveTRDays = _effectiveTemporaryResidenceDays(state.temporaryResidencePeriods);
    let result = _projectedApplicationDate(
        state.prStartDate, effectiveTRDays, state.absencePeriods);

    // Make sure counted temporary residence periods are still applicable.
    let applicableTRPeriods = state.temporaryResidencePeriods.slice();
    let earliestApplicableDate = DateMath.copy(result);
    earliestApplicableDate.setFullYear(result.getFullYear() - PRESENCE_PERIOD_IN_YEARS);
    while ((applicableTRPeriods.length > 0) &&
            (DateMath.compare(applicableTRPeriods[0].start, earliestApplicableDate) < 0)) {
        if (DateMath.compare(applicableTRPeriods[0].end, earliestApplicableDate) < 0) {
            // Skip the earliest period entirely.
            applicableTRPeriods = applicableTRPeriods.slice(1);
        } else {
            // Truncate the earliest period.
            applicableTRPeriods[0] = {
                start: earliestApplicableDate,
                end: applicableTRPeriods[0].end
            };
        }
        let newEffectiveTRDays = _effectiveTemporaryResidenceDays(applicableTRPeriods);
        if (newEffectiveTRDays !== effectiveTRDays) {
            assert(newEffectiveTRDays < effectiveTRDays);
            result = _projectedApplicationDate(
                state.prStartDate, newEffectiveTRDays, state.absencePeriods);
            earliestApplicableDate = DateMath.copy(result);
            earliestApplicableDate.setFullYear(result.getFullYear() - PRESENCE_PERIOD_IN_YEARS);
            effectiveTRDays = newEffectiveTRDays;
        }
    }
    return result;
}

function _sumPeriods(periods) {
    let result = 0;
    periods.forEach(period => {
        result += DateMath.durationInDays(period.start, period.end);
    });
    return result;
}

function _effectiveTemporaryResidenceDays(periods) {
    let temporaryResidenceDays = _sumPeriods(periods);
    return Math.min(
        Math.floor(temporaryResidenceDays / 2),
        MAXIMUM_CONVERTED_TEMPORARY_RESIDENCE_DAYS
    );
}

// It's not entirely clear how to calculate day of getting PR status and day of
// application. Current implementation is consistent with the official
// calculator though human interpretation can be different. It is recommended
// to apply with more than the minimum requirement.
function _projectedApplicationDate(prStartDate, temporaryResidenceDays, absencePeriods) {
    // At first calculate application date without absence days. Then go
    // through the absence periods and move application date forward gradually.
    // This is done to avoid including in the calculation absence periods that
    // will happen after the application date.
    let result = DateMath.copy(prStartDate);
    DateMath.adjustByDays(result, +REQUIRED_DAYS - temporaryResidenceDays);
    let absenceI = 0;
    while (absenceI < absencePeriods.length) {
        let period = absencePeriods[absenceI];
        if (DateMath.compare(period.start, result) <= 0) {
            DateMath.adjustByDays(result, DateMath.durationInDays(period.start, period.end));
        } else {
            break;
        }
        absenceI++;
    }
    return result;
}

function _applicationDateSummary(applicationDate) {
    let now = new Date(),
        isPast = (DateMath.compare(applicationDate, now) <= 0),
        verbWording = isPast ? 'have satisfied' : 'will satisfy';
    return (<span>You {verbWording} the physical presence requirements on <span className="ApplicationDate">{formatDate(applicationDate)}</span>.</span>);
}

function _applicationDateExplanation(applicationDate, canonicalState) {
    assert(applicationDate, 'Require application date to explain it');
    let result = {
        'requiredDays': REQUIRED_DAYS,
        'applicationDate': applicationDate,
    };

    let earliestApplicableDate = DateMath.copy(applicationDate);
    earliestApplicableDate.setFullYear(
        applicationDate.getFullYear() - PRESENCE_PERIOD_IN_YEARS);
    result['temporaryResidencePeriods'] = _keepPeriodsAfterDate(
        canonicalState.originalTemporaryResidencePeriods, earliestApplicableDate);
    result['ignoredTemporaryResidencePeriods'] = _keepPeriodsBeforeDate(
        canonicalState.originalTemporaryResidencePeriods, earliestApplicableDate);
    result['temporaryResidenceAbsencePeriods'] = _keepPeriodsAfterDate(
        canonicalState.effectiveTemporaryResidenceAbsencePeriods, earliestApplicableDate);

    let trDays = _sumPeriods(canonicalState.temporaryResidencePeriods);
    result['temporaryResidenceDays'] = trDays;
    trDays = Math.floor(trDays / 2);
    result['adjustedTemporaryResidenceDays'] = trDays;
    if (trDays > MAXIMUM_CONVERTED_TEMPORARY_RESIDENCE_DAYS) {
        result['cappedTemporaryResidenceDays'] = MAXIMUM_CONVERTED_TEMPORARY_RESIDENCE_DAYS;
    }

    let now = new Date();
    if (DateMath.compare(canonicalState.prStartDate, now) < 0) {
        result['permanentResidenceDays'] = DateMath.durationInDays(
            canonicalState.prStartDate, now);
        result['permanentResidenceStartDate'] = DateMath.copy(canonicalState.prStartDate);
    }
    // Here we should truncate at the end of the day of application date, i.e.
    // perform truncation including the end date.  _keepPeriodsBeforeDate isn't
    // doing that but application date should never be on absence day, so no
    // extra date adjusting is required.
    let absencePeriods = _keepPeriodsBeforeDate(canonicalState.absencePeriods, applicationDate);
    // It is a better user experience when today is considered as past. For
    // example, when you get PR status today, to tell 'You were a permanent
    // resident for 1 day.' For that purpose perform past/future truncation
    // using tomorrow date. So everything before tomorrow considered as already
    // happened.
    let tomorrow = DateMath.copy(now);
    DateMath.adjustByDays(tomorrow, 1);
    result['permanentResidenceAbsencePeriods'] = _keepPeriodsBeforeDate(absencePeriods, tomorrow);
    result['futurePermanentResidenceAbsencePeriods'] = _keepPeriodsAfterDate(absencePeriods, tomorrow);

    return result;
}

export default App;
export { expectedApplicationDate, canonicalState };
