import { DateMath } from './DateMath';

describe('getDaysInMonth', () => {
    it('regular month', () => {
        expect(DateMath.getDaysInMonth(2017, 9)).toEqual(31);
    });
    it('February non-leap', () => {
        expect(DateMath.getDaysInMonth(2017, 1)).toEqual(28);
    });
    it('February leap', () => {
        expect(DateMath.getDaysInMonth(2016, 1)).toEqual(29);
    });
    it('February non-leap tricky', () => {
        expect(DateMath.getDaysInMonth(2100, 1)).toEqual(28);
    });
});

describe('compare', () => {
    it('different years', () => {
        expect(DateMath.compare(new Date(2016, 9, 13), new Date(2017, 9, 13)))
            .toBeLessThan(0);
    });
    it('different months', () => {
        expect(DateMath.compare(new Date(2017, 8, 13), new Date(2017, 9, 13)))
            .toBeLessThan(0);
    });
    it('different days', () => {
        expect(DateMath.compare(new Date(2017, 9, 12), new Date(2017, 9, 13)))
            .toBeLessThan(0);
    });
    it('same dates', () => {
        expect(DateMath.compare(new Date(2017, 9, 13), new Date(2017, 9, 13)))
            .toEqual(0);
    });
});

describe('adjustByDays', () => {
    it('within month', () => {
        let date = new Date(2017, 9, 13);
        DateMath.adjustByDays(date, -2);
        expect(date.getFullYear()).toEqual(2017);
        expect(date.getMonth()).toEqual(9);
        expect(date.getDate()).toEqual(11);
    });

    it('on month boundary', () => {
        let date = new Date(2017, 9, 31);
        DateMath.adjustByDays(date, 2);
        expect(date.getFullYear()).toEqual(2017);
        expect(date.getMonth()).toEqual(10);
        expect(date.getDate()).toEqual(2);
    });

    it('on year boundary', () => {
        let date = new Date(2017, 11, 31);
        DateMath.adjustByDays(date, 1);
        expect(date.getFullYear()).toEqual(2018);
        expect(date.getMonth()).toEqual(0);
        expect(date.getDate()).toEqual(1);
    });

    it('in February, non-leap year', () => {
        let date = new Date(2017, 1, 26);
        DateMath.adjustByDays(date, 3);
        expect(date.getFullYear()).toEqual(2017);
        expect(date.getMonth()).toEqual(2);
        expect(date.getDate()).toEqual(1);
    });

    it('in February, leap year', () => {
        let date = new Date(2016, 1, 26);
        DateMath.adjustByDays(date, 3);
        expect(date.getFullYear()).toEqual(2016);
        expect(date.getMonth()).toEqual(1);
        expect(date.getDate()).toEqual(29);
    });
});

describe('moveToNextDay', () => {
    it('within month', () => {
        let date = new Date(2017, 9, 13);
        DateMath.moveToNextDay(date);
        expect(date.getFullYear()).toEqual(2017);
        expect(date.getMonth()).toEqual(9);
        expect(date.getDate()).toEqual(14);
    });
});

describe('copy', () => {
    it('returns equal date', () => {
        let date = new Date(2017, 9, 13);
        let copy = DateMath.copy(date);
        expect(copy.getFullYear()).toEqual(2017);
        expect(copy.getMonth()).toEqual(9);
        expect(copy.getDate()).toEqual(13);
    });

    it('returns a new independent object', () => {
        let date = new Date(2017, 9, 13);
        let copy = DateMath.copy(date);
        copy.setDate(1);
        expect(date.getDate()).toEqual(13);
    });
});

describe('durationInDays', () => {
    it('within month', () => {
        let from = new Date(2017, 9, 13),
            to = new Date(2017, 9, 17);
        expect(DateMath.durationInDays(from, to)).toEqual(5);
    });

    it('same date', () => {
        let date = new Date(2017, 9, 13);
        expect(DateMath.durationInDays(date, date)).toEqual(1);
    });

    it('on month boundary', () => {
        let from = new Date(2017, 9, 31),
            to = new Date(2017, 10, 2);
        expect(DateMath.durationInDays(from, to)).toEqual(3);
    });

    it('on year boundary', () => {
        let from = new Date(2017, 11, 30),
            to = new Date(2018, 0, 2);
        expect(DateMath.durationInDays(from, to)).toEqual(4);
    });

    it('over multiple years', () => {
        let from = new Date(2016, 11, 31),
            to = new Date(2018, 0, 1);
        expect(DateMath.durationInDays(from, to)).toEqual(367);
    });

    it('over February, non-leap year', () => {
        let from = new Date(2017, 1, 1),
            to = new Date(2017, 2, 1);
        expect(DateMath.durationInDays(from, to)).toEqual(29);
    });

    it('over February, leap year', () => {
        let from = new Date(2016, 1, 1),
            to = new Date(2016, 2, 1);
        expect(DateMath.durationInDays(from, to)).toEqual(30);
    });
});

describe('durationInNaturalUnits', () => {
    it('within month', () => {
        let from = new Date(2017, 9, 13),
            to = new Date(2017, 9, 17);
        expect(DateMath.durationInNaturalUnits(from, to)).toEqual(
            {years: 0, months: 0, days: 4});
    });

    it('same date', () => {
        let date = new Date(2017, 9, 13);
        expect(DateMath.durationInNaturalUnits(date, date)).toEqual(
            {years: 0, months: 0, days: 0});
    });

    it('on year boundary', () => {
        let from = new Date(2017, 11, 30),
            to = new Date(2018, 0, 2);
        expect(DateMath.durationInNaturalUnits(from, to)).toEqual(
            {years: 0, months: 0, days: 3});
    });

    it('all units', () => {
        let from = new Date(2017, 0, 3),
            to = new Date(2018, 2, 13);
        expect(DateMath.durationInNaturalUnits(from, to)).toEqual(
            {years: 1, months: 2, days: 10});
    });

    it('over February, non-leap year', () => {
        let from = new Date(2017, 1, 20),
            to = new Date(2017, 2, 1);
        expect(DateMath.durationInNaturalUnits(from, to)).toEqual(
            {years: 0, months: 0, days: 9});
    });

    it('over February, leap year', () => {
        let from = new Date(2016, 1, 20),
            to = new Date(2016, 2, 1);
        expect(DateMath.durationInNaturalUnits(from, to)).toEqual(
            {years: 0, months: 0, days: 10});
    });

    it('exact number of years', () => {
        let from = new Date(2015, 9, 13),
            to = new Date(2017, 9, 13);
        expect(DateMath.durationInNaturalUnits(from, to)).toEqual(
            {years: 2, months: 0, days: 0});
    });

    it('exact number of months', () => {
        let from = new Date(2016, 0, 13),
            to = new Date(2016, 9, 13);
        expect(DateMath.durationInNaturalUnits(from, to)).toEqual(
            {years: 0, months: 9, days: 0});
    });
});
