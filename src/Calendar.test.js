import React from 'react';
import ReactTestUtils from 'react-dom/test-utils';

import Calendar from './Calendar';

var onSelectDateSpy = jasmine.createSpy('onSelectDateFunction');
beforeEach(() => {
    onSelectDateSpy = jasmine.createSpy('onSelectDateFunction');
});

function renderComponent(propsOverrides) {
    let defaultProps = {
        // First month of the 4th quarter.
        startDate: new Date(2017, 9, 1),
        endDate: new Date(2017, 9, 31),
        selectable: true,
        onSelectDate: onSelectDateSpy,
    };
    let props = Object.assign({}, defaultProps, propsOverrides || {});
    return ReactTestUtils.renderIntoDocument(
        <Calendar {...props}/>
    );
}

describe('date selection', () => {
    it('is not propagated if calendar not selectable', () => {
        const componentTree = renderComponent({selectable: false});
        let dayCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
            componentTree, 'Day');
        ReactTestUtils.Simulate.click(dayCells[1]);
        expect(onSelectDateSpy).not.toHaveBeenCalled();
    });

    it('click on actual day', () => {
        const componentTree = renderComponent({selectable: true});
        let dayCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
            componentTree, 'Day');
        ReactTestUtils.Simulate.click(dayCells[1]);

        expect(onSelectDateSpy).toHaveBeenCalled();
        let args = onSelectDateSpy.calls.mostRecent().args;
        expect(args.length).toEqual(1);
        let date = args[0];
        expect(date.getFullYear()).toEqual(2017);
        expect(date.getMonth()).toEqual(9);
        expect(date.getDate()).toEqual(2);
    });

    it('click on table row', () => {
        const componentTree = renderComponent({selectable: true});
        let rows = ReactTestUtils.scryRenderedDOMComponentsWithTag(
            componentTree, 'tr');
        ReactTestUtils.Simulate.click(rows[0]);
        expect(onSelectDateSpy).not.toHaveBeenCalled();
    });

    it('click on padding', () => {
        const componentTree = renderComponent({selectable: true});
        let rows = ReactTestUtils.scryRenderedDOMComponentsWithTag(
            componentTree, 'tr');
        ReactTestUtils.Simulate.click(rows[0].firstElementChild);
        expect(onSelectDateSpy).not.toHaveBeenCalled();
    });
});

describe('days before the startDate', () => {
    it('should be visually different', () => {
        const componentTree = renderComponent({startDate: new Date(2017, 9, 4)});
        let disabledDayCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
            componentTree, 'Day-disabled');
        expect(disabledDayCells.length).toEqual(3);
    });

    it('should ignore clicks', () => {
        const componentTree = renderComponent({
            startDate: new Date(2017, 9, 2), selectable: true});
        let disabledDayCell = ReactTestUtils.findRenderedDOMComponentWithClass(
            componentTree, 'Day-disabled');
        ReactTestUtils.Simulate.click(disabledDayCell);
        expect(onSelectDateSpy).not.toHaveBeenCalled();
    });
});

describe('show selected date', () => {
    it('single date', () => {
        const componentTree = renderComponent({selection: [
            {
                type: 'single',
                date: new Date(2017, 9, 13),
                className: 'visible-selection'
            },
            {
                type: 'single',
                date: new Date(2016, 9, 13),
                className: 'wrong-year'
            },
            {
                type: 'single',
                date: new Date(2017, 8, 13),
                className: 'wrong-month'
            },
        ]});
        let visibleSelectionCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
            componentTree, 'visible-selection');
        expect(visibleSelectionCells.length).toEqual(1);
        expect(visibleSelectionCells[0].getAttribute('data-day')).toEqual('13');
        let wrongYearCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
            componentTree, 'wrong-year');
        expect(wrongYearCells.length).toEqual(0);
        let wrongMonthCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
            componentTree, 'wrong-month');
        expect(wrongMonthCells.length).toEqual(0);
    });

    it('date range', () => {
        const componentTree = renderComponent({selection: [
            {
                type: 'range',
                start: new Date(2017, 9, 12),
                end: new Date(2017, 9, 14),
                className: 'visible-selection'
            },
        ]});
        let visibleSelectionCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
            componentTree, 'visible-selection');
        expect(visibleSelectionCells.length).toEqual(3);
        expect(visibleSelectionCells[0].getAttribute('data-day')).toEqual('12');
        expect(visibleSelectionCells[1].getAttribute('data-day')).toEqual('13');
        expect(visibleSelectionCells[2].getAttribute('data-day')).toEqual('14');
        // start/end classes.
        expect(visibleSelectionCells[0].classList.contains('visible-selection-start')).toBeTruthy();
        expect(visibleSelectionCells[0].classList.contains('visible-selection-end')).toBeFalsy();
        expect(visibleSelectionCells[1].classList.contains('visible-selection-start')).toBeFalsy();
        expect(visibleSelectionCells[1].classList.contains('visible-selection-end')).toBeFalsy();
        expect(visibleSelectionCells[2].classList.contains('visible-selection-start')).toBeFalsy();
        expect(visibleSelectionCells[2].classList.contains('visible-selection-end')).toBeTruthy();
    });

    it('overlapping single date and range', () => {
        const componentTree = renderComponent({selection: [
            {
                type: 'single',
                date: new Date(2017, 9, 13),
                className: 'selection-single'
            },
            {
                type: 'range',
                start: new Date(2017, 9, 12),
                end: new Date(2017, 9, 14),
                className: 'selection-range'
            },
        ]});
        let singleSelectionCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
            componentTree, 'selection-single');
        expect(singleSelectionCells.length).toEqual(1);
        expect(singleSelectionCells[0].classList.contains('selection-range')).toBe(true);
    });

    it('should not modify selection', () => {
        let selectedDates = [
            {
                type: 'range',
                start: new Date(2017, 9, 12),
                end: new Date(2017, 9, 14),
                className: 'selection-range'
            },
        ];
        const componentTree = renderComponent({selection: selectedDates});
        let startDate = selectedDates[0].start;
        expect(startDate.getFullYear()).toEqual(2017);
        expect(startDate.getMonth()).toEqual(9);
        expect(startDate.getDate()).toEqual(12);
        let endDate = selectedDates[0].end;
        expect(endDate.getFullYear()).toEqual(2017);
        expect(endDate.getMonth()).toEqual(9);
        expect(endDate.getDate()).toEqual(14);
    });
});

describe('pending range selection', () => {
    it('should always show at least single day', () => {
        const componentTree = renderComponent({
            pendingRangeSelection: new Date(2017, 9, 13)});
        let selectionCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
            componentTree, 'pendingPeriodSelection-single');
        expect(selectionCells.length).toEqual(1);
        expect(selectionCells[0].getAttribute('data-day')).toEqual('13');
    });

    describe('hover', () => {
        var calendar, dayCells;
        function renderComponentAndGetElements(calendarProps) {
            const componentTree = renderComponent(calendarProps);
            calendar = ReactTestUtils.findRenderedDOMComponentWithClass(
                componentTree, 'Calendar');
            dayCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
                componentTree, 'Day');
            return componentTree;
        }

        it('before pending selection', () => {
            const componentTree = renderComponentAndGetElements({
                pendingRangeSelection: new Date(2017, 9, 13)});
            ReactTestUtils.Simulate.mouseEnter(calendar);
            ReactTestUtils.Simulate.mouseMove(dayCells[1]);
            let selectionCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
                componentTree, 'pendingPeriodSelection');
            expect(selectionCells.length).toEqual(12);
            expect(selectionCells[0].getAttribute('data-day')).toEqual('2');
            expect(selectionCells[selectionCells.length - 1].getAttribute('data-day'))
                .toEqual('13');
        });

        it('after pending selection', () => {
            const componentTree = renderComponentAndGetElements({
                pendingRangeSelection: new Date(2017, 9, 13)});
            ReactTestUtils.Simulate.mouseEnter(calendar);
            ReactTestUtils.Simulate.mouseMove(dayCells[19]);
            let selectionCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
                componentTree, 'pendingPeriodSelection');
            expect(selectionCells.length).toEqual(8);
            expect(selectionCells[0].getAttribute('data-day')).toEqual('13');
            expect(selectionCells[selectionCells.length - 1].getAttribute('data-day'))
                .toEqual('20');
        });

        it('without pending selection', () => {
            const componentTree = renderComponentAndGetElements();
            ReactTestUtils.Simulate.mouseEnter(calendar);
            ReactTestUtils.Simulate.mouseMove(dayCells[1]);
            let selectionCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
                componentTree, 'pendingPeriodSelection');
            expect(selectionCells.length).toEqual(0);
        });

        describe('with mouse outside', () => {
            it('initially', () => {
                const componentTree = renderComponentAndGetElements({
                    pendingRangeSelection: new Date(2017, 9, 13)});
                //ReactTestUtils.Simulate.mouseEnter(calendar);
                ReactTestUtils.Simulate.mouseMove(dayCells[1]);
                let selectionCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
                    componentTree, 'pendingPeriodSelection');
                expect(selectionCells.length).toEqual(0);
            });

            it('explicitly moved out', () => {
                const componentTree = renderComponentAndGetElements({
                    pendingRangeSelection: new Date(2017, 9, 13)});
                ReactTestUtils.Simulate.mouseEnter(calendar);
                ReactTestUtils.Simulate.mouseLeave(calendar);
                ReactTestUtils.Simulate.mouseMove(dayCells[1]);
                let selectionCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
                    componentTree, 'pendingPeriodSelection');
                expect(selectionCells.length).toEqual(0);
            });
        });
    });

    describe('inactive days', () => {
        var calendar;
        function renderComponentAndGetElements(pendingRangeSelection) {
            const componentTree = renderComponent({
                startDate: new Date(2017, 9, 3),
                endDate: new Date(2017, 10, 30),
                pendingRangeSelection: pendingRangeSelection,
            });
            calendar = ReactTestUtils.findRenderedDOMComponentWithClass(
                componentTree, 'Calendar');
            return componentTree;
        }

        describe('day before start. replace with start date', () => {
            var componentTree, disabledCells;
            beforeEach(() => {
                componentTree = renderComponentAndGetElements(new Date(2017, 9, 13));
                disabledCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
                    componentTree, 'Day-disabled');
            });

            it('click', () => {
                ReactTestUtils.Simulate.click(disabledCells[0]);
                expect(onSelectDateSpy).toHaveBeenCalled();
                let args = onSelectDateSpy.calls.mostRecent().args;
                expect(args.length).toEqual(1);
                let date = args[0];
                expect(date.getFullYear()).toEqual(2017);
                expect(date.getMonth()).toEqual(9);
                expect(date.getDate()).toEqual(3);
            });

            it('hover', () => {
                ReactTestUtils.Simulate.mouseEnter(calendar);
                ReactTestUtils.Simulate.mouseMove(disabledCells[0]);
                let selectionCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
                    componentTree, 'pendingPeriodSelection');
                expect(selectionCells.length).toEqual(11);
                expect(selectionCells[0].getAttribute('data-day')).toEqual('3');
                expect(selectionCells[selectionCells.length - 1].getAttribute('data-day'))
                    .toEqual('13');
            });
        });

        describe('padding', () => {
            var componentTree, cells;

            describe('month start before padding', () => {
                beforeEach(() => {
                    componentTree = renderComponentAndGetElements(new Date(2017, 10, 13));
                    cells = ReactTestUtils.scryRenderedDOMComponentsWithTag(
                        componentTree, 'td');
                });

                it('click', () => {
                    // Next cell after 6 weeks of October.
                    ReactTestUtils.Simulate.click(cells[42]);
                    expect(onSelectDateSpy).toHaveBeenCalled();
                    let args = onSelectDateSpy.calls.mostRecent().args;
                    expect(args.length).toEqual(1);
                    let date = args[0];
                    expect(date.getFullYear()).toEqual(2017);
                    expect(date.getMonth()).toEqual(10);
                    expect(date.getDate()).toEqual(1);
                });

                it('hover', () => {
                    ReactTestUtils.Simulate.mouseEnter(calendar);
                    // Next cell after 6 weeks of October.
                    ReactTestUtils.Simulate.mouseMove(cells[42]);
                    let selectionCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
                        componentTree, 'pendingPeriodSelection');
                    expect(selectionCells.length).toEqual(13);
                    expect(selectionCells[0].getAttribute('data-day')).toEqual('1');
                    expect(selectionCells[selectionCells.length - 1].getAttribute('data-day'))
                        .toEqual('13');
                });
            });

            describe('month start after padding', () => {
                beforeEach(() => {
                    componentTree = renderComponentAndGetElements(new Date(2017, 9, 13));
                    cells = ReactTestUtils.scryRenderedDOMComponentsWithTag(
                        componentTree, 'td');
                });

                it('click', () => {
                    // Next cell after 6 weeks of October.
                    ReactTestUtils.Simulate.click(cells[42]);
                    expect(onSelectDateSpy).toHaveBeenCalled();
                    let args = onSelectDateSpy.calls.mostRecent().args;
                    expect(args.length).toEqual(1);
                    let date = args[0];
                    expect(date.getFullYear()).toEqual(2017);
                    expect(date.getMonth()).toEqual(9);
                    expect(date.getDate()).toEqual(31);
                });

                it('hover', () => {
                    ReactTestUtils.Simulate.mouseEnter(calendar);
                    // Next cell after 6 weeks of October.
                    ReactTestUtils.Simulate.mouseMove(cells[42]);
                    let selectionCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
                        componentTree, 'pendingPeriodSelection');
                    expect(selectionCells.length).toEqual(19);
                    expect(selectionCells[0].getAttribute('data-day')).toEqual('13');
                    expect(selectionCells[selectionCells.length - 1].getAttribute('data-day'))
                        .toEqual('31');
                });
            });

            describe('month end before padding', () => {
                beforeEach(() => {
                    componentTree = renderComponentAndGetElements(new Date(2017, 10, 13));
                    cells = ReactTestUtils.scryRenderedDOMComponentsWithTag(
                        componentTree, 'td');
                });

                it('click', () => {
                    // Last cell in 6 weeks of October.
                    ReactTestUtils.Simulate.click(cells[41]);
                    expect(onSelectDateSpy).toHaveBeenCalled();
                    let args = onSelectDateSpy.calls.mostRecent().args;
                    expect(args.length).toEqual(1);
                    let date = args[0];
                    expect(date.getFullYear()).toEqual(2017);
                    expect(date.getMonth()).toEqual(10);
                    expect(date.getDate()).toEqual(1);
                });

                it('hover', () => {
                    ReactTestUtils.Simulate.mouseEnter(calendar);
                    // Last cell in 6 weeks of October.
                    ReactTestUtils.Simulate.mouseMove(cells[41]);
                    let selectionCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
                        componentTree, 'pendingPeriodSelection');
                    expect(selectionCells.length).toEqual(13);
                    expect(selectionCells[0].getAttribute('data-day')).toEqual('1');
                    expect(selectionCells[selectionCells.length - 1].getAttribute('data-day'))
                        .toEqual('13');
                });
            });

            describe('month end after padding', () => {
                beforeEach(() => {
                    componentTree = renderComponentAndGetElements(new Date(2017, 9, 13));
                    cells = ReactTestUtils.scryRenderedDOMComponentsWithTag(
                        componentTree, 'td');
                });

                it('click', () => {
                    // Last cell in 6 weeks of October.
                    ReactTestUtils.Simulate.click(cells[41]);
                    expect(onSelectDateSpy).toHaveBeenCalled();
                    let args = onSelectDateSpy.calls.mostRecent().args;
                    expect(args.length).toEqual(1);
                    let date = args[0];
                    expect(date.getFullYear()).toEqual(2017);
                    expect(date.getMonth()).toEqual(9);
                    expect(date.getDate()).toEqual(31);
                });

                it('hover', () => {
                    ReactTestUtils.Simulate.mouseEnter(calendar);
                    // Last cell in 6 weeks of October.
                    ReactTestUtils.Simulate.mouseMove(cells[41]);
                    let selectionCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
                        componentTree, 'pendingPeriodSelection');
                    expect(selectionCells.length).toEqual(19);
                    expect(selectionCells[0].getAttribute('data-day')).toEqual('13');
                    expect(selectionCells[selectionCells.length - 1].getAttribute('data-day'))
                        .toEqual('31');
                });
            });
        });

        describe('padding before start', () => {
            var componentTree, cells;
            beforeEach(() => {
                componentTree = renderComponentAndGetElements(new Date(2017, 9, 13));
                cells = ReactTestUtils.scryRenderedDOMComponentsWithTag(
                    componentTree, 'td');
            });

            it('click', () => {
                // First cell in October.
                ReactTestUtils.Simulate.click(cells[0]);
                expect(onSelectDateSpy).toHaveBeenCalled();
                let args = onSelectDateSpy.calls.mostRecent().args;
                expect(args.length).toEqual(1);
                let date = args[0];
                expect(date.getFullYear()).toEqual(2017);
                expect(date.getMonth()).toEqual(9);
                expect(date.getDate()).toEqual(3);
            });

            it('hover', () => {
                ReactTestUtils.Simulate.mouseEnter(calendar);
                // First cell in October.
                ReactTestUtils.Simulate.mouseMove(cells[0]);
                let selectionCells = ReactTestUtils.scryRenderedDOMComponentsWithClass(
                    componentTree, 'pendingPeriodSelection');
                expect(selectionCells.length).toEqual(11);
                expect(selectionCells[0].getAttribute('data-day')).toEqual('3');
                expect(selectionCells[selectionCells.length - 1].getAttribute('data-day'))
                    .toEqual('13');
            });
        });
    });
});
