import React, { Component } from 'react';
import './DateInputField.css';

/**
 * Input field that manages selected state separately. Designated for dates.
 *
 * Props:
 * - value: string value to show in the field;
 * - selected: boolean that marks if the field should be shown as selected;
 * - onFocus: function without parameters, called when the field gets focus.
 */
class DateInputField extends Component {
    constructor(props) {
        super(props);
        this._handleFocus = this._handleFocus.bind(this);
        this._setInputElementRef = this._setInputElementRef.bind(this);
        this._inputElement = null;
    }

    _handleFocus(event) {
        event.stopPropagation();
        this.props.onFocus();
    }

    _preventClickPropagation(event) {
        event.stopPropagation();
    }

    render() {
        let className = 'DateInputField';
        if (this.props.selected) {
            className += ' selected';
        }
        return (
            <div className={className}>
                <input
                    type="text"
                    maxLength="10"
                    placeholder="Select date"
                    value={this.props.value}
                    readOnly
                    onFocus={this._handleFocus}
                    onClick={this._preventClickPropagation}
                    ref={this._setInputElementRef}
                />
            </div>
        );
    }

    _setInputElementRef(element) {
        this._inputElement = element;
    }

    focus() {
        if (this._inputElement && (this._inputElement !== document.activeElement)) {
            this._inputElement.focus();
        }
    }
}

export default DateInputField;
