import React, { Component } from 'react';
import './ImportExportModal.css';

/**
 * Component to display a modal window with import/export field.
 *
 * Props:
 * - visible: boolean.
 * - content: string.
 * - onImport: function with 1 parameter. The single argument is user-provided text.
 * - onCancel: function with no parameters.
 */
class ImportExportModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.content,
            error: '',
        };

        this._handleOutOfModalClick = this._handleOutOfModalClick.bind(this);
        this._handleImportClick = this._handleImportClick.bind(this);
        this._handleChange = this._handleChange.bind(this);
    }

    _handleOutOfModalClick(event) {
        event.stopPropagation();
        let clickedClass = event.target.className;
        if ((clickedClass === 'Backdrop') || (clickedClass === 'ContentContainer')) {
            this.props.onCancel();
        }
    }

    _handleImportClick() {
        this.setState(prevState => {
            let errorMessage = this.props.onImport(prevState.value);
            return {error: errorMessage};
        });
    }

    _handleChange(event) {
        this.setState({value: event.target.value});
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            value: nextProps.content,
            error: '',
        });
    }

    render() {
        let classNames = "Modal";
        if (this.props.visible) {
            classNames += " Modal-visible";
        }
        let errorElement = null;
        if (this.state.error) {
            errorElement = (
                <div className="Error">
                    {this.state.error}
                </div>
            );
        }
        return (
            <div className={classNames} onClick={this._handleOutOfModalClick}>
                <div className="Backdrop"></div>
                <div className="ContentContainer">
                    <div className="Content">
                        <div className="Header">Import/Export</div>
                        <div className="Body">
                            Your data isn't transmitted and isn't stored anywhere.
                            To export the data please copy the text below and save
                            it in a file on your machine. To import the data later
                            you can paste the saved data and click Import.
                            <textarea
                                value={this.state.value}
                                onChange={this._handleChange}
                                rows="12"
                            ></textarea>
                            {errorElement}
                        </div>
                        <div className="Footer">
                            <button onClick={this.props.onCancel}>Cancel</button>
                            <button onClick={this._handleImportClick}>Import</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ImportExportModal;
