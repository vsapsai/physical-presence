import React from 'react';
import ReactDOM from 'react-dom';
import ReactTestUtils from 'react-dom/test-utils';

import DateInputField from './DateInputField';

it('displays provided value', () => {
    const componentTree = ReactTestUtils.renderIntoDocument(
        <DateInputField value="testing" />);
    let input = ReactTestUtils.findRenderedDOMComponentWithTag(
        componentTree, 'input');
    expect(input.value).toEqual('testing');
});

describe('marks selected state', () => {
    it('unselected', () => {
        const componentTree = ReactTestUtils.renderIntoDocument(
            <DateInputField />);
        ReactTestUtils.findRenderedDOMComponentWithClass(
            componentTree, 'DateInputField');
        let selected = ReactTestUtils.scryRenderedDOMComponentsWithClass(
            componentTree, 'selected');
        expect(selected.length).toEqual(0);
    });

    it('selected', () => {
        const componentTree = ReactTestUtils.renderIntoDocument(
            <DateInputField selected="true"/>);
        let selected = ReactTestUtils.scryRenderedDOMComponentsWithClass(
            componentTree, 'DateInputField selected');
        expect(selected.length).toEqual(1);
    });
});

it('calls onFocus props callback', () => {
    let onFocusSpy = jasmine.createSpy('onFocusFunction');
    const componentTree = ReactTestUtils.renderIntoDocument(
        <DateInputField onFocus={onFocusSpy} />);
    let input = ReactTestUtils.findRenderedDOMComponentWithTag(
        componentTree, 'input');
    ReactTestUtils.Simulate.focus(input);
    expect(onFocusSpy).toHaveBeenCalled();
});
